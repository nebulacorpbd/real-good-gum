import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    background: {
        label: 'Background',
        type: ElementPropTypes.image
    }
};

export const defaultConfig = {
    background: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1505325522/59b6bdbe668bf8001114c08d/xkrygjdttmdwvrnw2x4x.png',
    }
};
