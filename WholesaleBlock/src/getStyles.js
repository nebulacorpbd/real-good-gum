export const getStyles = (globalStyles, blockConfig) => ({
	WholesaleWrapper: {
    overflow: 'auto',
		position: 'relative',
		zIndex: 1,
	},
	ObjectFit: {
		objectFit: 'cover',
    objectPosition: 'center',
    height: '100%',
    width: '100%',
    maxWidth: 'unset',
    position: 'absolute',
    top: 0,
    left: 0,
		zIndex: -1,
	},
	Container: {
		display: 'flex',
    padding: '0 15px',
    maxWidth: '820px',
		margin: 'auto',
		'@media (max-width: 960px)': {
      display: 'block',
		},
	},
  Column12: {
    flex: '1 1 100%',
    width: '100%',
		textAlign: 'center'
  },
	PageHeader: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#0047ba',
    fontSize: '42px',
    lineHeight: '1.1em',
    fontWeight: 700,
    fontStyle: 'normal',
		textAlign: 'center',
    margin: '24px auto 12px',
    boxSizing: 'border-box',
	},
	WholesaleParagraph: {
    margin: '0 0 12px',
		fontSize: '16px',
    lineHeight: '1.4',
    letterSpacing: '.16px',
    boxSizing: 'border-box',
		color: '#0047ba',
		'@media (max-width: 768px)': {
    	margin: '12px auto',
    	lineHeight: '160%',
		}
	},
  WholesaleForm: {
    width: '100%'
  }
});
