import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

class Block extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
    }
  }

  componentDidMount() {
    this.setState({loaded: true});
  }
  render() {
    const styles = StyleSheet.create(getStyles({}, this.props));
    if ( this.state.loaded ) {
      return (
        <div className={css(styles.WholesaleWrapper)}>
          <img className={css(styles.ObjectFit)} src={this.props.background.uriBase + 'q_auto,f_auto/' + this.props.background.imagePath} />
          <div className={css(styles.Container)}>
            <div className={css(styles.Column12)}>
              <h1 className={css(styles.PageHeader)}>Wholesale Registration</h1>
              <p className={css(styles.WholesaleParagraph)}>
                Our wholesale program is for registered businesses to purchase cases of Real Good Gum at a wholesale discount. If you think you might be interested then fill out the form and we will be in contact soon. Thank you for your interest in becoming a wholesale business customer!
              </p>
              <iframe height="1300" allowtransparency="true" frameBorder="0" scrolling="no" className={css(styles.WholesaleForm)} rel="width:100%;border:none" src='https://realgoodgum.wufoo.com/embed/z8ayl9m0d4dp9b/'></iframe>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className={css(styles.WholesaleWrapper)}>
          <img className={css(styles.ObjectFit)} src={this.props.background.uriBase + 'q_auto,f_auto/' + this.props.background.imagePath} />
          <div className={css(styles.Container)}>
            <div className={css(styles.Column12)}>
              <h1 className={css(styles.PageHeader)}>Wholesale Registration</h1>
              <p className={css(styles.WholesaleParagraph)}>
                Our wholesale program is for registered businesses to purchase cases of Real Good Gum at a wholesale discount. If you think you might be interested then fill out the form and we will be in contact soon. Thank you for your interest in becoming a wholesale business customer!
              </p>
            </div>
          </div>
        </div>
      );
    }
  }
}
Block.defaultProps = defaultConfig;

export default Block;
