import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

function FooterBlock(props) {
  const styles = StyleSheet.create(getStyles({}, props));

  return (
    <React.Fragment>
      <div className={css(styles.Wrapper)}>
        <div className={css(styles.Container)}>
          <p className={css(styles.Copyright)}>{props.copyright}</p>
        </div>
      </div>
    </React.Fragment>
  )
}

FooterBlock.defaultProps = defaultConfig;

export default FooterBlock;
