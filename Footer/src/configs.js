import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    copyright: {
        label: 'Copyright Text Content',
        type: ElementPropTypes.string
    }
};

export const defaultConfig = {
    copyright: '© 2017 Real Good Brand Inc.  |  2802 Flintrock Trace, Ste. 2006  |  Austin, Texas 78738'
};
