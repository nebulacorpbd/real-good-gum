export const getStyles = (globalStyles, blockConfig) => ({
	Wrapper: {
    padding: '30px 0',
    backgroundColor: "#0047ba",
	},
	Container: {
		display: 'flex',
    padding: '1px 8px',
    maxWidth: '820px',
		margin: 'auto',
		textAlign: 'center',
		'@media (max-width: 768px)': {
      display: 'block',
		},
	},
	Copyright: {
		fontFamily: 'Nunito',
		fontSize: '13px',
		lineHeight: '2',
		fontWeight: '700',
		letterSpacing: '.01em',
    padding: '0 15px',
    margin: '0 auto',
		color: '#ffffff',
	},
});
