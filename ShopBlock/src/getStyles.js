export const getStyles = (globalStyles, blockConfig) => ({
  shopTitleWrapper: {
    padding: '15px',
    display: 'flex',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    flexDirection: 'column',
    '@media (max-width: 600px)': {
      padding: 0,
    }
  },
  shopTitle: {
    display: 'block',
    fontFamily: 'Lobster Hand',
    fontSize: '42px',
    letterSpacing: '1.5px',
    margin: '0 auto',
    lineHeight: 1.1,
    color: '#0047ba',
    paddingTop: '45px',
    paddingBottom: '12px',
    textAlign: 'center',
    '@media (max-width: 599px)': {
      width: '100%',
      borderBottom: '1px solid #0047ba',
    }
  },
  shopCategory: {
    listStyle: 'none',
    textAlign: 'center',
    width: '100%',
    borderBottom: '1px solid #0047ba',
    padding: '5px',
    '@media (max-width: 599px)': {
      textAlign: 'left',
      padding: 0,
      margin: 0,
    }
  },
  shopCategoryItem: {
    '@media (min-width: 600px)': {
      display: 'inline-block',
      fontSize: '19px',
      padding: '0',
      lineHeight: 1.2,
      margin: '0 20px',
      color: '#0047ba',
      cursor: 'pointer',
      ':hover': {
        color: '#ff3fb4',
        borderBottom: '1px solid #ff3fb4',
      },
    },
    '@media (max-width: 599px)': {
      display: 'block',
      borderBottom: '1px solid #0047ba',
      padding: '0 15px 0 30px',
      fontSize: '19px',
      lineHeight: 2.5,
      margin: 0,
      backgroundColor: 'transparent',
      color: '#0047ba',
      textDecoration: 'none',
      ':hover': {
        backgroundColor: '#ff3fb4',
        color: '#fff',
      },
    }
  },
  shopCategoryItemActive: {
    position: 'relative',
    '@media (min-width: 600px)': {
      color: '#ff3fb4',
      borderBottom: '1px solid #ff3fb4',
    },
    '@media (max-width: 599px)': {
      display: 'block',
      borderBottom: '1px solid #0047ba',
      padding: '0 15px 0 30px',
      fontSize: '19px',
      lineHeight: 2.5,
      margin: 0,
      backgroundColor: '#ff3fb4',
      color: '#fff',
      textDecoration: 'none',
    }
  },
  ProductCardWrapper: {
    maxWidth: '300px',
    margin: 'auto',
    position: 'relative',
    display: 'block',
    padding: '8px',
  },
  rightArrow: {
    fontSize: '26px',
    position: 'absolute',
    left: '10px',
    top: '50%',
    lineHeight: '1em',
    transform: 'translateY(-50%) scaleX(.5)',
    fontWeight: 600,
    '@media (min-width: 600px)': {
      display: 'none',
    }
  },
  shopCategoryItemTitle: {
    fontSize: '19px',
    fontWeight: 900,
    color: '#0047ba',
    textDecoration: 'none',
    '@media (min-width: 600px)': {
      display: 'inline-block',
      padding: '0',
      margin: '0 20px',
      lineHeight: 2,
    },
    '@media (max-width: 599px)': {
      display: 'block',
      borderBottom: '1px solid #0047ba',
      padding: '0 15px',
      lineHeight: 2.5,
      margin: 0,
      backgroundColor: 'transparent',
    }
  },
  shopFilterWrapper: {
    padding: '0 15px',
    overflow: 'auto',
  },
  shopItemCount: {
    color: '#0047ba',
    lineHeight: 2,
    fontSize: '19px',
    margin: '15px 0',
    float: 'left',
    '@media (max-width: 600px)': {
      float: 'none',
    }
  },
  productsWrapper: {
    padding: '8px',
    display: 'flex',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  ProductCardItem: {
    display: 'block',
    flex: '1 1 50%',
    maxWidth: '25%',
    textAlign: 'center',
    marginBottom: '30px',
    padding: '8px',
    position: 'relative',
    '@media (max-width: 1279px) and (min-width: 960px)': {
      flex: '1 1 33.333333%',
      maxWidth: '33.333333%',
    },
    '@media (max-width: 959px) and (min-width: 600px)': {
      flex: '1 1 50%',
      maxWidth: '50%',
    },
    '@media (max-width: 599px)': {
      flex: '1 1 50%',
      maxWidth: '50%',
    },
  },
  ProductCardImage: {
    marginBottom: '15px',
    maxHeight: '300px',
    maxWidth: '100%',
    border: 0,
    margin: 'auto',
    transition: 'opacity .2s ease',
  },
  ProductCardTitle: {
    fontSize: '24px',
    fontFamily: 'Lobster Hand',
    letterSpacing: '1.5px',
    fontWeight: 700,
    lineHeight: 1.5,
    color: '#0047ba',
  },
  ProductCardPrice: {
    fontSize: '19px',
    color: '#0047ba',
    lineHeight: 1.4,
  },
  ProductCardOptions: {
    fontSize: '87%',
    color: '#0047ba',
  },
  ProductCardLink: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    color: '#ff3fb4',
    width: '100%',
    height: '100%',
    margin: 'auto',
    transition: 'background .2s ease',
    opacity: 0,
    zIndex: 2,
    ':hover': {
      background: 'rgba(255,255,255,.82)',
      opacity: 1,
    }
  },
  ProductCardLinkText: {
    fontFamily: 'Lobster Hand',
    letterSpacing: '2px',
    fontSize: '30px',
    position: 'absolute',
    top: '40%',
    right: 0,
    left: 0,
    display: 'block',
    transform: 'translate3d(0, -50%, 0)',
    opacity: 1,
  },
  shopFilterSorting: {
    fontSize: '19px',
    margin: '15px 0',
    lineHeight: 2,
    fontWeight: 'bold',
    letterSpacing: '0.095em',
    textTransform: 'uppercase',
    display: 'flex',
    float: 'right',
    '@media (max-width: 600px)': {
      float: 'none',
    }
  },
  shopFilterSortingMenu: {
    appearance: 'none',
    maxWidth: '100%',
    border: '2px solid #0047ba',
    padding: '5px 35px 5px 10px',
    marginLeft: '10px',
    color: '#0047ba',
    cursor: 'pointer',
    fontSize: '18.24px',
    lineHeight: '1.4em',
    outline: 'none',
    borderRadius: 0,
    backgroundColor: '#fff',
    backgroundImage: 'url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4IiB3aWR0aD0iNDgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTE0IDIwbDEwIDEwIDEwLTEweiIvPjxwYXRoIGQ9Ik0wIDBoNDh2NDhoLTQ4eiIgZmlsbD0ibm9uZSIvPjwvc3ZnPg==")',
    backgroundRepeat: 'no-repeat',
    backgroundSize: '25px',
    backgroundPosition: 'calc(100% - 6px) center',
  }
})
