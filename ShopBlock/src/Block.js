import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

class ShopBlock extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: props.data.items || [],
        sortOption: 'newest',
      }
    }
    fetchData = (sortOption) => {
      this.props.utils.client.products.getByCategoryId({ categoryId: '', sort: sortOption }).then(data => {
        if ( data.items != null) {
          this.setState({ data: data.items })
        }
      }).catch(console.error);
    }
    componentDidMount() {
      this.setState({ data: this.props.data.items || [] })
    }
    handleSortChange = (e) => {
      const { value } = e.target;
      this.setState({sortOption: value});
      this.fetchData(value);
    };
    render(){
      const styles = StyleSheet.create(getStyles({}, this.props));
      const {data} = this.state;
      if ( data != [] ) {
        return (
            <React.Fragment>
              <div className={css(styles.shopWrapper)}>
                <div className={css(styles.shopTitleWrapper)}>
                  <h1 className={css(styles.shopTitle)}>SHOP</h1>
                  <ul className={css(styles.shopCategory)}>
                    <li className={css(styles.shopCategoryItemTitle)}>Shop:</li>
                    <li className={css(styles.shopCategoryItem, styles.shopCategoryItemActive)}><span className={css(styles.rightArrow)}>></span>Shop</li>
                    <li className={css(styles.shopCategoryItem)}>All Shop</li>
                  </ul>
                </div>
                <div className={css(styles.shopFilterWrapper)}>
                  <p className={css(styles.shopItemCount)}>Showing items 1–{data.length} of {data.length} total items</p>
                  <div className={css(styles.shopFilterSorting)}>
                    SORT BY
                    <select
                      value={this.state.sortOption}
                      className={css(styles.shopFilterSortingMenu)}
                      onChange={this.handleSortChange}>
                      <option value="highest price">Highest Price</option>
                      <option value="lowest price">Lowest Price</option>
                      <option value="name z-a">Name Z-A</option>
                      <option value="name a-z">Name A-Z</option>
                      <option value="newest">Newest</option>
                    </select>
                  </div>
                </div>
                <ul className={css(styles.productsWrapper)}>
                  {data.map( (item, index) => {
                    return <li key={index} className={css(styles.ProductCardItem)}>
                      <a href={"/p/" + item.seo_friendlyName} className={css(styles.ProductCardLink)}>
                        <span className={css(styles.ProductCardLinkText)}>DETAILS</span>
                      </a>
                      <div className={css(styles.ProductCardWrapper)}>
                        <div>
                          <img src={(item.images[0].uriBase != null ) ? item.images[0].uriBase + 'q_auto,f_auto/' + item.images[0].imagePath : ""} className={css(styles.ProductCardImage)} />
                        </div>
                        <div className={css(styles.ProductCardDescription)}>
                          <div className={css(styles.ProductCardTitle)}>{item.name}</div>
                          <div className={css(styles.ProductCardPrice)}>${item.price}</div>
                          <div className={css(styles.ProductCardOptions)}>{(item.variantOptions.length > 0)? "More options available":""}</div>
                        </div>
                      </div>
                    </li>
                  })}
                </ul>
              </div>
            </React.Fragment>
        );
      } else {
          <React.Fragment>
            <div className={css(styles.shopWrapper)}>
              <div className={css(styles.shopTitleWrapper)}>
                <h1 className={css(styles.shopTitle)}>SHOP</h1>
                <ul className={css(styles.shopCategory)}>
                  <li className={css(styles.shopCategoryItemTitle)}>Shop:</li>
                  <li className={css(styles.shopCategoryItem, styles.shopCategoryItemActive)}>Shop</li>
                  <li className={css(styles.shopCategoryItem)}>All Shop</li>
                </ul>
              </div>
              <div className={css(styles.shopFilterWrapper)}>
                  <h1 className={css(styles.shopTitle)}>Shop is loading</h1>
              </div>
            </div>
          </React.Fragment>
      }
    }
}

export default ShopBlock;
