export const getStyles = (globalStyles, blockConfig) => ({
	Wrapper: {
    padding: '65px 0',
    backgroundColor: "#ff3fb4",
		position: 'relative',
		zIndex: 1,
	},
	ObjectFit: {
		objectFit: 'cover',
    objectPosition: 'center',
    height: '100%',
    width: '100%',
    maxWidth: 'unset',
    position: 'absolute',
    top: 0,
    left: 0,
		zIndex: -1,
	},
	Container: {
		display: 'flex',
    padding: '8px',
    maxWidth: '820px',
		margin: 'auto',
		'@media (max-width: 768px)': {
      display: 'block',
			textAlign: 'center'
		},
	},
  Column3: {
    flex: '1 1 25%',
    width: '25%',
		'@media (max-width: 768px)': {
      flex: '1 1 100%%',
      width: '100%',
		},
  },
  Column9: {
    flex: '1 1 75%%',
    width: '75%',
		'@media (max-width: 768px)': {
      flex: '1 1 100%%',
      width: '100%',
		},
  },
	Header: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#FFF',
    fontWeight: 700,
    fontStyle: 'normal',
		textAlign: 'center',
	},
	SmallHeader: {
    lineHeight: '1.1em',
    fontSize: '42px',
    fontWeight: 700,
    fontStyle: 'normal',
    marginTop: '12px',
    marginBottom: '0px',
		'@media (max-width: 768px)': {
    	fontSize: '42px',
		}
	},
	BigHeader: {
    fontSize: '64px',
    letterSpacing: '1px',
    marginTop: 0,
    marginBottom: '30px',
    lineHeight: 1,
	},
	Paragraph: {
		color: '#FFF',
    marginBottom: '17px',
    paddingLeft: '10px',
    paddingRight: '10px',
    width: '80%',
    margin: '0 auto 0 0',
		fontFamily: 'Nunito',
		fontSize: '16px',
    lineHeight: '1.4',
		fontWeight: '700',
    letterSpacing: '.01em',
		'@media (max-width: 768px)': {
    	margin: '12px auto',
			width: '90%',
		}
	},
	Link: {
		color: '#ffe900 !important',
		':hover': {
			textDecoration: 'underline',
		},
		'@media (max-width: 768px)': {
    	marginLeft: 'auto',
			width: '90%',
		}
	},
	SectionButton: {
    fontFamily: 'Lobster Hand',
    letterSpacing: '1px',
    fontSize: '24px',
		display: 'inline-block',
    paddingLeft: '10px',
    paddingRight: '10px',
    marginTop: '12px',
    marginBottom: '12px',
    lineHeight: 1.1,
    color: '#ffe900',
    width: '80%',
		':hover': {
    	textDecoration: 'underline',
    	transform: 'scale(1.1) rotate(-1deg)',
		}
	}
});
