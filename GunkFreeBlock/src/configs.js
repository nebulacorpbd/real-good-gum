import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    headerPartOne: {
        label: 'Header Upper',
        type: ElementPropTypes.string
    },
    headerPartTwo: {
        label: 'Header Lower',
        type: ElementPropTypes.string
    },
    background: {
        label: 'Background',
        type: ElementPropTypes.image
    },
};

export const defaultConfig = {
    headerPartOne: 'WHAT THE',
    headerPartTwo: 'GUNK?!',
    background: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751194/59b6bdbe668bf8001114c08d/pebzymhjdjlrtpnm11oy.png',
    },
};
