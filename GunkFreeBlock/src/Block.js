import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

function GunkFreeBlock(props) {
  const styles = StyleSheet.create(getStyles({}, props));

  return (
    <React.Fragment>
      <div className={css(styles.Wrapper)} id="gunk-free">
        <picture className={css(styles.ObjectFit)}>
          <source media="(max-width: 480px)" srcset={props.background.uriBase + 'w_480,h_519,c_fill,q_auto:good,f_auto/' + props.background.imagePath} />
          <source media="(min-width: 481px) and (max-width: 960px)" srcset={props.background.uriBase + 'w_960,h_519,c_fill,q_auto:good,f_auto/' + props.background.imagePath} />
          <img className={css(styles.ObjectFit)} src={props.background.uriBase + 'c_limit,q_auto:good,f_auto/' + props.background.imagePath} />
        </picture>
        <div className={css(styles.Container)}>
          <div className={css(styles.Column3)}>
            <h3 className={css(styles.Header, styles.SmallHeader)}>{props.headerPartOne}</h3>
            <h3 className={css(styles.Header, styles.BigHeader)}>{props.headerPartTwo}</h3>
          </div>
          <div className={css(styles.Column9)}>
            <p className={css(styles.Paragraph)}>
              Being healthy means paying attention to what you put in your body. And your kids bodies. And that means getting rid of artificial ingredients. This means getting rid of the aspartame or other artificial sweeteners typically found in other chewing gums. Plus, you probably didn’t know but most other gums also include plastic, neurotoxins and carcinogens. All hidden in a innocent enough looking ingredient called “<a className={css(styles.Link)} href="https://www.youtube.com/watch?v=XaCmy3vGYEI" target="_blank">gum base</a>”. We don’t have any of that. Our gum is made from just 5 simple ingredients from the forest, farm and garden. All sustainably harvested. And not a better gum to chew but also a better gum for you. And the planet. Unlike other chewing gums, all Real Good Gum is 100% biodegradable.<br/>
              You can enjoy our gum knowing that all our gums are soy, nut and peanut free, non-gmo, gluten-free, vegan, natural and organic and biodegradable.<br/>
              Happy chewing!
            </p>
            <a className={css(styles.SectionButton)} href="/page/glossary-of-gunk">GET THE FULL DOWNLOAD WITH OUR GLOSSARY OF GUNK.</a>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

GunkFreeBlock.defaultProps = defaultConfig;

export default GunkFreeBlock;
