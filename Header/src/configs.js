import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
  altLogo: {
    label: 'Alternate Logo',
    type: ElementPropTypes.image
  },
};
export const defaultConfig = {
  altLogo: {
    uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
    imagePath: 'v1585252704/5d2cc53fad73cbc3375b091c/hl8jxys2ekhw2nyievqa.png',
  },
};
