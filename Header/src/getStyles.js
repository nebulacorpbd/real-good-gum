export const getStyles = (globalStyles, blockConfig) => ({
	headerWrapper: {
		position: 'relative',
		height: 'auto',
		textAlign: 'center',
    padding: '8px 15px',
    width: '100%',
		fontSize: 0,
    background: '#FFFFFF',
    zIndex: 999999,
		transition: 'transform 200ms linear',
		'@media (max-width: 959px)': {
			position: 'fixed',
			top: 0,
			borderBottom: '1px solid #0047ba',
		}
	},
	HeaderWrapperMain: {
		'@media (max-width: 959px)': {
			height: '83px'
		}
	},
	headerSticky: {
		position: 'fixed',
		height: 'auto',
		textAlign: 'center',
    padding: '8px 15px',
    width: '100%',
    background: '#FFFFFF',
    zIndex: 999999,
		top: 0,
		transition: 'transform 200ms linear',
		'@media (max-width: 959px)': {
			borderBottom: '1px solid #0047ba',
		}
	},
	stickyDown: {
		transform: 'translateY(-100%)'
	},
	menuWrapper: {
		padding: '8px',
		margin: '0',
		textAlign: 'center',
    position: 'absolute',
    top: '50%',
    left: 0,
    right: 0,
    transform: 'translateY(-50%)',
		'@media (max-width: 959px)': {
			display: 'none',
		},
	},
	responsiveMenu: {
		position: 'fixed',
		top: 0,
		left: 0,
		height: '100%',
		width: '100%',
		margin: 0,
		textAlign: 'center',
		zIndex: 9999999,
		backgroundColor: 'rgba(255, 255, 255, 0.97)',
		padding: '75px 20px 15px',
		transform: 'translateY(-100%)',
    overflow: 'auto',
		transition: 'all .2s ease',
		'@media (min-width: 960px)': {
			display: 'none',
		},
	},
	responsiveMenuOn: {
		transform: 'translateY(0%)',
	},
	menuItem: {
    display: 'inline-block',
    verticalAlign: 'middle',
    padding: '0 15px',
    whiteSpace: 'nowrap',
	},
	responsiveMenuItem: {
    display: 'block',
		width: '100%',
    whiteSpace: 'nowrap',
		fontSize: '24px',
		letterSpacing: '2px',
		padding: '15px 12px',
	},
	menuClose: {
    position: 'absolute',
    left: '15px',
    top: '10px',
		width: '35px',
		height: '35px',
	},
	menuClose1: {
    position: 'absolute',
    transformOrigin: 'center',
		display: 'block',
		width: '35px',
		height: '3px',
		top: '50%',
		backgroundColor: 'rgb(0, 71, 186)',
		transform: 'rotate(45deg)',
	},
	menuClose2: {
    position: 'absolute',
    transformOrigin: 'center',
		display: 'block',
		width: '35px',
		height: '3px',
		top: '50%',
		backgroundColor: 'rgb(0, 71, 186)',
		transform: 'rotate(-45deg)',
	},
	menuCart: {
		position: 'absolute',
		padding: '8px',
		top: '50%',
		right: '15px',
		transform: 'translateY(-50%)',
		float: 'right',
	},
	menuToogle: {
		position: 'absolute',
		padding: '15.5px 8px',
		top: '50%',
		left: '15px',
		transform: 'translateY(-50%)',
		float: 'left',
		minHeight: '50px',
		'@media (min-width: 960px)': {
			display: 'none',
		}
	},
	hamburgerMenu: {
		display: 'block',
		width: '25px',
		height: '3px',
		backgroundColor: '#0047ba',
		transitionDuration: '.3s',
	},
	hamburgerMenuMiddle: {
    marginTop: '5px',
    marginBottom: '5px',
	},
	menuItemRight: {
    display: 'inline-block',
    verticalAlign: 'middle',
    padding: '0 15px',
    whiteSpace: 'nowrap',
		'@media (min-width: 960px)': {
			marginRight: '180px',
		}
	},
	menuLink: {
		fontFamily: 'Lobster Hand',
    display: 'inline-block',
    padding: '10px 0',
    color: '#0047ba',
    backgroundColor: 'transparent',
		textDecoration: 'none',
    fontSize: '20px',
    letterSpacing: '2px',
    transition: 'color .2s ease, transform 0s',
    ':hover': {
		  color: '#ff3fb4',
    	transform: 'scale(1.1) rotate(2deg)',
    },
		'@media (max-width: 959px)': {
			fontSize: '24px',
			padding: '0',
		}
  },
	menuOpenCart: {
		fontFamily: '"Nunito", sans-serif',
    display: 'inline-block',
    padding: '10px 0',
    color: '#0047ba',
    backgroundColor: 'transparent',
		textDecoration: 'none',
    fontSize: '16px',
    letterSpacing: '2px',
    transition: 'color .2s ease, fill .2s ease, transform 0s',
		fill: '#0047ba',
    ':hover': {
		  color: '#ff3fb4',
			fill: '#ff3fb4',
    	transform: 'scale(1.1) rotate(2deg)',
    },
  },
	cartIconHeader: {
		marginBottom: '-3px',
	},
	logoWrapper: {
    width: 'auto',
    height: 'auto',
		maxHeight: '136px',
		display: 'inline-block',
		padding: '8px',
    transition: '0s',
    position: 'relative',
    zIndex: '1000',
		margin: 'auto',
		boxSizing: 'border-box',
		overflow: 'hidden',
    ':hover': {
    	transform: 'scale(1.1) rotate(2deg)',
		},
		'::after': {
			content: 'hello',
		},
		'@media (max-width: 959px)': {
				maxHeight: '66px',
		}
	},
	logo: {
    width: 'auto',
    height: '120px',
    // transition: 'transform 200ms linear 200ms',
    // transform: 'translateY(100%)'
		'@media (max-width: 959px)': {
				height: '50px',
		}
	},
	logoSticky: {
    display: 'none',
	},
	logoAlt: {
    display: 'block',
    width: '85px',
    height: '60px',
    margin: '0 auto',
	},
});
