import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

class HeaderBlock extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: props.data.menu || [],
        logoUrl: (props.utils.client.storeInformation.logo.uriBase + 'c_fill,q_auto,f_auto/' + this.props.utils.client.storeInformation.logo.imagePath) || '',
        menuToggle: false,
        sticky: false,
        scrollDown: false,
        headerClass: "",
      }
    }
    openCart = () => this.props.utils.pubSub.publish(this.props.utils.events.cart.openCart);
    toggleMenu = () => this.setState({ menuToggle: !this.state.menuToggle });
    componentDidMount() {
      const { queryParams, utils, data } = this.props;
      const {
          events,
          isAmpRequest,
          pubSub
      } = utils;
      const { isRendering } = data;
      if (isRendering) {
        pubSub.subscribe(
           events.cart.replyWithTotalItems,
           this.handleItemAdded
        );
        pubSub.subscribe(
           events.cart.updateCartCount,
           this.handleItemAdded
        );
        this.askForTotalItems();
      } else this.setState({ totalItems: 0 });
      this.oldScroll = window.scrollY;
      window.addEventListener('scroll', e => this.handleScroll(e));
    }
    componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
    }
    handleScroll = (e) => {
      let scrollTop = window.scrollY;
      if ( scrollTop > 100 ) {
        this.setState({sticky: true});
        if ( this.oldScroll > scrollTop ){
          this.setState({scrollDown: false});
        } else if (this.oldScroll < scrollTop) {
          this.setState({scrollDown: true});
        }
      } else {
        this.setState({sticky: false});
      }
      this.oldScroll = window.scrollY;
    }
    askForTotalItems = () => {
      const { pubSub, events } = this.props.utils;
      const totalItemsReturned = this.state.totalItems >= 0;
      if(!totalItemsReturned) {
        setTimeout(() => {
          pubSub.publish(events.cart.askForTotalItems, null);
          this.askForTotalItems();
        }, 100);
      }
    };
    handleItemAdded = (msg, totalItems) => {
      this.setState({totalItems: totalItems});
    }
    render() {
      const styles = StyleSheet.create(getStyles({}, this.props));
      let count = this.state.totalItems || 0;
      return (
          <React.Fragment>
            <div className={css(styles.HeaderWrapperMain)}>
              <div className={
                (this.state.sticky) ? css(this.state.scrollDown && styles.stickyDown, styles.headerSticky) : css(styles.headerWrapper)}>
                <a href="/" className={css(styles.logoWrapper)}>
                  <picture className={(this.state.sticky) ? css(styles.logoSticky) : css(styles.logo)}>
                    <source media="(max-width: 480px)" srcSet={this.state.logoUrl} />
                    <source media="(min-width: 481px) and (max-width: 960px)" srcSet={this.state.logoUrl} />
                    <img className={(this.state.sticky) ? css(styles.logoSticky) : css(styles.logo)} src={this.state.logoUrl} />
                  </picture>
                  <picture className={css(styles.logoAlt)}>
                    <source media="(max-width: 480px)" srcSet={this.props.altLogo.uriBase + 'c_fill,q_auto,f_auto/' + this.props.altLogo.imagePath} />
                    <source media="(min-width: 481px) and (max-width: 960px)" srcSet={this.props.altLogo.uriBase + 'c_fill,q_auto,f_auto/' + this.props.altLogo.imagePath} />
                    <img className={css(styles.logoAlt)} src={this.props.altLogo.uriBase + 'c_fill,q_auto,f_auto/' + this.props.altLogo.imagePath} />
                  </picture>
                </a>
                <div className={css(styles.menuItem, styles.menuToogle)}>
                  <a href="#" onClick={() => this.toggleMenu()}>
                    <span className={css(styles.hamburgerMenu)}></span>
                    <span className={css(styles.hamburgerMenu, styles.hamburgerMenuMiddle)}></span>
                    <span className={css(styles.hamburgerMenu)}></span>
                  </a>
                </div>
                <ul className={css(styles.menuWrapper)}>
                  {this.state.data.map( (item, index) => {
                    return <li key={index} className={index !== (this.state.data.length/2-1) ? css(styles.menuItem) : css(styles.menuItemRight)}>
                      <a href={item.url} onClick={() => this.setState({menuToggle: false})} className={css(styles.menuLink)}>
                        {item.name}
                      </a>
                    </li>;
                  })}
                </ul>
                <ul className={css(styles.responsiveMenu, (this.state.menuToggle) ? styles.responsiveMenuOn : '' )} >
                  <a href="#" onClick={() => this.setState({menuToggle: false})} className={css(styles.menuClose)}>
                    <span className={css(styles.menuClose1)}></span>
                    <span className={css(styles.menuClose2)}></span>
                  </a>
                  {this.state.data.map( (item, index) => {
                    return <li key={index} className={css(styles.responsiveMenuItem)}>
                      <a href={item.url} className={css(styles.menuLink)}>
                        {item.name}
                      </a>
                    </li>;
                  })}
                </ul>
                <div className={css(styles.menuItem, styles.menuCart)}>
                  <a href="#" className={css(styles.menuOpenCart)} onClick={() => this.openCart()}>
                    <svg id="CartIcon" width="19px" height="19px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" className={css(styles.cartIconHeader)}>
                      <path d="M461.5,222h-411c-11.046,0-20,8.954-20,20l40,207c0,11.046,8.954,20,20,20h331c11.046,0,20-8.954,20-20l40-207  C481.5,230.954,472.546,222,461.5,222z M138,403.5c0,5.799-4.701,10.5-10.5,10.5H127.5c-5.799,0-10.5-4.701-10.5-10.5v-117  c0-5.799,4.701-10.5,10.5-10.5h0.001c5.799,0,10.5,4.701,10.5,10.5V403.5z M204,403.5c0,5.799-4.701,10.5-10.5,10.5H193.5  c-5.799,0-10.5-4.701-10.5-10.5v-117c0-5.799,4.701-10.5,10.5-10.5h0.001c5.798,0,10.5,4.701,10.5,10.5V403.5z M266,403.5  c0,5.799-4.701,10.5-10.5,10.5H255.5c-5.799,0-10.5-4.701-10.5-10.5v-117c0-5.799,4.701-10.5,10.5-10.5h0.001  c5.798,0,10.5,4.701,10.5,10.5V403.5z M331,403.5c0,5.799-4.701,10.5-10.5,10.5s-10.5-4.701-10.5-10.5v-117  c0-5.799,4.701-10.5,10.5-10.5s10.5,4.701,10.5,10.5V403.5z M396,403.5c0,5.799-4.701,10.5-10.5,10.5s-10.5-4.701-10.5-10.5v-117  c0-5.799,4.701-10.5,10.5-10.5s10.5,4.701,10.5,10.5V403.5z"/>
                      <path d="M416.595,121.376c-3.04-25.57-25.181-47.13-50.66-50.02c-7.088-0.521-30.334-0.401-46.035-0.348  C318.899,60.897,310.373,53,300,53h-87c-10.387,0-18.92,7.919-19.901,18.049c-19.155-0.169-49.697-0.793-60.374,3.647  c-19.07,7.12-34.01,24.74-37.04,44.99c-4.64,29.089-9.399,58.169-13.91,87.291c6.721,0.029,13.44,0.159,20.16-0.021  c4.53-27.64,8.83-55.31,13.34-82.95c1.83-16.4,15.96-30.24,32.12-32.61c6.937-0.618,30.914-0.396,46.275-0.343  C195.911,99.648,203.703,106,213,106h87c9.31,0,17.11-6.37,19.34-14.982c14.012,0.012,34.461,0.038,39.715-0.012  c18.1-0.27,35.21,14.37,37.569,32.34c4.561,27.86,8.82,55.77,13.471,83.61c6.7,0.21,13.42-0.011,20.13,0.01  C425.805,178.416,421.114,149.916,416.595,121.376z"/>
                    </svg> {this.state.totalItems}
                  </a>
                </div>
              </div>
            </div>
          </React.Fragment>
      );
    }
}
HeaderBlock.defaultProps = defaultConfig;

export default HeaderBlock;
