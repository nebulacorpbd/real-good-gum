export const getDataProps = (utils, props) => {
	return utils.client.menus.get()
	  .then( (data) => {
			return {menu: data.items[0].items, isRendering: utils.isRendering}
	  })
	  .catch(console.error);
};
