export const getStyles = blockConfig => ({
    HeroWrapper: {
      height: '600px',
  		position: 'relative',
  		zIndex: 1,
  	},
  	ObjectFit: {
  		objectFit: 'cover',
      objectPosition: 'center',
      height: '100%',
      width: '100%',
      maxWidth: 'unset',
      position: 'absolute',
      top: 0,
      left: 0,
  		zIndex: -1,
  	},
});
