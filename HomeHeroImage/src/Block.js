import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

class Block extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        size: '',
      }
    }
    componentDidMount() {
      if (window.innerWidth >= 960) {
        this.setState({size: 'w_1750,c_fill,'});
      } else {
        this.setState({size: 'w_960,c_fill,'});
      }
    }
    render() {
      const styles = StyleSheet.create(getStyles(defaultConfig, this.props));
      return (
          <React.Fragment>
            <div className={css(styles.HeroWrapper)}>
              <picture className={css(styles.ObjectFit)}>
                <source media="(max-width: 480px)" srcSet={this.props.image.uriBase + this.state.size +'w_480,h_600,c_fill,q_auto,f_auto/' + this.props.image.imagePath} />
                <source media="(min-width: 481px) and (max-width: 960px)" srcSet={this.props.image.uriBase + this.state.size +'w_480,h_600,c_fill,q_auto,f_auto/' + this.props.image.imagePath} />
                <img className={css(styles.ObjectFit)} src={this.props.image.uriBase + this.state.size +'h_600,q_auto,f_auto/' + this.props.image.imagePath} />
              </picture>
            </div>
          </React.Fragment>
      );
    }
}
Block.defaultProps = defaultConfig;

export default Block;
