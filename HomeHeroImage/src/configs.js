import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    image: {
        label: 'Background',
        type: ElementPropTypes.image
    }
};

export const defaultConfig = {
    image: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1584280808/5d2cc53fad73cbc3375b091c/aafl2rjtbaargmf5rqqc.png',
    }
};
