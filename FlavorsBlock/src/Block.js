import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

class FlavorsBlock extends React.Component {
  constructor(props){
    super(props);
  }
  markdown = (el, style) => {
    return el.split("").map((letter, i) => (
      <span key={i} className={style}>{letter}</span>
    ))
  }

  render(){
    const styles = StyleSheet.create(getStyles({}, this.props));
    const { props } = this;
    return (
      <React.Fragment>
        <div className={css(styles.Wrapper)} id="flavors">
          <div className={css(styles.FlavorsContainer)}>
            <div className={css(styles.Column12, styles.flexRow)}>
              <div className={css(styles.FirstHeader)}>
                <h2 className={css(styles.HeaderBlue, styles.FlavorsNonResponsive)}>{this.markdown(defaultConfig.header1, css(styles.MarkdownSourceHeader))}</h2>
                <h2 className={css(styles.HeaderBlue, styles.FlavorsResponsive)}>{this.markdown("NO ASPARTAME.", css(styles.MarkdownSourceHeader))}</h2>
                <h2 className={css(styles.HeaderBlue, styles.FlavorsResponsive, styles.FlavorsHeader96)}>{this.markdown("PLASTIC FREE.", css(styles.MarkdownSourceHeader))}</h2>
              </div>
              <div className={css(styles.SecondHeader)}>
                <div className={css(styles.flexSecondHeader)}>
                  <h3 className={css(styles.HeaderWhite, styles.FlavorsNonResponsive)}>{this.markdown(defaultConfig.header2, css(styles.MarkdownSourceHeader))}</h3>
                  <h3 className={css(styles.HeaderWhite, styles.FlavorsResponsive)}>
                    {this.markdown("ALWAYS GUNK-FREE", css(styles.MarkdownSourceHeader))}
                    <strong className={css(styles.RMark)}>®</strong>
                  </h3>
                  <h3 className={css(styles.HeaderWhite, styles.FlavorsResponsive)}>{this.markdown("AND", css(styles.MarkdownSourceHeader))}</h3>
                  <h3 className={css(styles.HeaderWhite, styles.FlavorsResponsive)}>{this.markdown("ALWAYS FREE SHIPPING", css(styles.MarkdownSourceHeader))}</h3>
                </div>
              </div>
            </div>
          </div>

          <div className={css(styles.Container)}>
            <div className={css(styles.Column4)}>
              <a href="/p/natural-chicle-bubble-gum" className={css(styles.ProductShowcase)} >
                <picture className={css(styles.ProductShowThumbnail)}>
                  <source media="(max-width: 480px)" srcSet={props.product1Image.uriBase + 'w_480,c_fill,q_auto,f_auto/' + props.product1Image.imagePath} />
                  <source media="(min-width: 481px) and (max-width: 960px)" srcSet={props.product1Image.uriBase + 'h_350,c_fill,q_auto,f_auto/' + props.product1Image.imagePath} />
                  <img className={css(styles.ProductShowThumbnail)} src={props.product1Image.uriBase + 'h_350,c_fill,q_auto,f_auto/' + props.product1Image.imagePath} />
                </picture>
                <div className={css(styles.ProductShowDesc)}>
                  <h3 className={css(styles.ProductShowTitle)}>{this.markdown("Hello, Bubbleful", css(styles.Markdown))}<strong className={css(styles.Nunito)}>®</strong></h3>
                  <div className={css(styles.ProductShowButton)}>SHOP NOW</div>
                </div>
              </a>
            </div>
            <div className={css(styles.Column4)}>
              <a href="/p/natural-chicle-gum-mint" className={css(styles.ProductShowcase)} >
                <picture className={css(styles.ProductShowThumbnail)}>
                  <source media="(max-width: 480px)" srcSet={props.product2Image.uriBase + 'w_480,c_fill,q_auto,f_auto/' + props.product2Image.imagePath} />
                  <source media="(min-width: 481px) and (max-width: 960px)" srcSet={props.product2Image.uriBase + 'h_350,c_fill,q_auto,f_auto/' + props.product2Image.imagePath} />
                  <img className={css(styles.ProductShowThumbnail)} src={props.product2Image.uriBase + 'h_350,c_fill,q_auto,f_auto/' + props.product2Image.imagePath} />
                </picture>
                <div className={css(styles.ProductShowDesc)}>
                  <h3 className={css(styles.ProductShowTitle)}>{this.markdown("Nice to mint you", css(styles.Markdown))}<strong className={css(styles.Nunito)}>™</strong></h3>
                  <div className={css(styles.ProductShowButton)}>SHOP NOW</div>
                </div>
              </a>
            </div>
            <div className={css(styles.Column4)}>
              <a href="/p/natural-chicle-gum-cinnamon" className={css(styles.ProductShowcase)} >
                <picture className={css(styles.ProductShowThumbnail)}>
                  <source media="(max-width: 480px)" srcSet={props.product3Image.uriBase + 'w_480,c_fill,q_auto,f_auto/' + props.product3Image.imagePath} />
                  <source media="(min-width: 481px) and (max-width: 960px)" srcSet={props.product3Image.uriBase + 'h_350,c_fill,q_auto,f_auto/' + props.product3Image.imagePath} />
                  <img className={css(styles.ProductShowThumbnail)} src={props.product3Image.uriBase + 'h_350,c_fill,q_auto,f_auto/' + props.product3Image.imagePath} />
                </picture>
                <div className={css(styles.ProductShowDesc)}>
                  <h3 className={css(styles.ProductShowTitle)}>{this.markdown("What's up, Cinnaman?", css(styles.Markdown))}<strong className={css(styles.Nunito)}>™</strong></h3>
                  <div className={css(styles.ProductShowButton)}>SHOP NOW</div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

FlavorsBlock.defaultProps = defaultConfig;

export default FlavorsBlock;
