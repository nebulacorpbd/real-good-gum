import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    header1: {
        label: 'Header One Content',
        type: ElementPropTypes.string
    },
    header2: {
        label: 'Header Two Content',
        type: ElementPropTypes.string
    },
    product1Image: {
        label: 'Product 1 Image',
        type: ElementPropTypes.image
    },
    product2Image: {
        label: 'Product 2 Image',
        type: ElementPropTypes.image
    },
    product3Image: {
        label: 'Product 3 Image',
        type: ElementPropTypes.image
    },
};

export const defaultConfig = {
    header1: 'NO ASPARTAME. PLASTIC FREE. ALWAYS GOODER.',
    header2: 'ALWAYS GUNK-FREE® AND ALWAYS FREE SHIPPING',
    product1Image: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751197/59b6bdbe668bf8001114c08d/d441i1rbadhto2ejst6t.png',
    },
    product2Image: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751197/59b6bdbe668bf8001114c08d/zjysk7ftsnajgpl9fqde.png',
    },
    product3Image: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751192/59b6bdbe668bf8001114c08d/vailt1aknytx43kabdbz.png',
    },
};
