export const getStyles = (globalStyles, blockConfig) => ({
	Wrapper: {
    padding: '54px 0 40px',
    backgroundColor: "#ffe900",
		position: 'relative',
		zIndex: 2,
	},
	Container: {
		position: 'relative',
		display: 'flex',
    padding: '8px',
		margin: 'auto',
		'@media (max-width: 959px)': {
      display: 'block',
		},
	},
	FlavorsContainer: {
		display: 'flex',
		margin: 'auto',
		'@media (max-width: 959px)': {
      display: 'block',
		},
	},
  Column4: {
    flex: '1 1 33.333333%',
    maxWidth: '33.333333%',
		textAlign: 'center',
		'@media (max-width: 959px)': {
	    flex: '1 1 100%',
			maxWidth: '100%',
		}
  },
  Column12: {
    flex: '1 1 100%',
    maxWidth: '100%',
		textAlign: 'center'
  },
	HeaderBlue: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#ffffff',
    fontSize: '42px',
    lineHeight: '1.1em',
		letterSpacing: '1.5px',
    fontStyle: 'normal',
		textAlign: 'center',
    marginTop: '10px',
    padding: '4px 40px 2px',
    border: 'solid #0047ba',
    borderWidth: '4.5px 3px 3px 4px',
    borderRadius: '50px/60px 40px',
    backgroundColor: '#0047ba',
		padding: '4px 20px 0',
		transform: 'translateX(-50%) rotate(-.75deg)',
		position: 'absolute',
		left: '50%',
		fontWeight: 700,
		top: '-24px',
		margin: 0,
    textRendering: 'optimizeLegibility',
		'@media (max-width: 600px)': {
			fontSize: '34px',
			top: '-44px',
		},
	},
	FlavorsHeader96: {
		'@media (max-width: 600px)': {
			transform: 'translateX(-50%) translateY(96%) rotate(-.75deg)',
		}
	},
	RMark: {
    fontFamily: '"Nunito",sans-serif',
    verticalAlign: 'super',
    fontSize: '50%',
	},
	FirstHeader: {
		padding: '8px',
		minHeight: '40px',
	},
	SecondHeader: {
		padding: '12px',
		whiteSpace: 'nowrap',
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		textAlign: 'center',
		'@media (max-width: 959px)': {
			marginTop: '20px',
		}
	},
	flexSecondHeader: {
		whiteSpace: 'nowrap',
		display: 'flex',
		flexDirection: 'column',
	},
	HeaderWhite: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#0047ba',
    fontSize: '36px',
    lineHeight: '1.1em',
    fontStyle: 'normal',
		textAlign: 'center',
		fontWeight: 400,
    margin: '0px',
    padding: '0px 20px 0px 20px',
    border: 'solid #fff',
    borderWidth: '4.5px 3px 3px 4px',
    borderTopLeftRadius: '100px 120px',
    borderTopRightRadius: '155px 100px',
    borderBottomRightRadius: '100px 120px',
    borderBottomLeftRadius: '120px 75px',
    transform: 'rotate(1deg)',
    backgroundColor: '#fff',
		display: 'inline-block'
	},
	FlavorsNonResponsive: {
		'@media (max-width: 959px)': {
			display: 'none',
		}
	},
	FlavorsResponsive: {
		'@media (min-width: 960px)': {
			display: 'none',
		},
		'@media (max-width: 959px)': {
	    margin: '0px auto',
			display: 'inline-block',
		}
	},
	Paragraph: {
		color: '#FFF',
    marginBottom: '17px',
    paddingLeft: '10px',
    paddingRight: '10px',
    width: '75%',
    margin: '0 auto',
		fontSize: '16px',
    lineHeight: '1.4',
		fontWeight: '700',
    letterSpacing: '.01em',
		'@media (max-width: 959px)': {
    	margin: '12px auto',
			width: '90%',
    	lineHeight: '160%',
		}
	},
	Link: {
		color: '#ffe900 !important',
		':hover': {
			textDecoration: 'underline',
		},
		'@media (max-width: 959px)': {
    	marginLeft: 'auto',
			width: '90%',
		}
	},
	flexRow: {
		padding: 0,
		'@media (max-width: 959px)': {
			display: 'flex',
			flexDirection: 'column',
		}
	},
	ProductShowcase: {
		display: 'block',
		width: '100%',
		margin: '40px auto',
		':hover': {
    	transform: 'rotate(2deg) scale(1.1)',
		}
	},
	Nunito: {
		fontFamily: '"Nunito", sans-serif',
		fontSize: '50%',
    verticalAlign: 'super',
	},
	ProductShowDesc: {
		':hover': {
    	transform: 'rotate(-4deg) scale(1.2)',
		}
	},
	ProductShowTitle: {
    fontFamily: 'Lobster Hand',
    fontSize: '26px',
    letterSpacing: '1.5px',
    whiteSpace: 'nowrap',
		margin: '24px auto 12px',
		color: '#ff3fb4',
		fontWeight: 400,
    ':nth-of-type(2)': {
			fontSize: '105%',
    },
	},
	Markdown: {
		position: 'relative',
    ':nth-of-type(2)': {
			fontSize: '105%',
    },
    ':nth-of-type(3)': {
			fontSize: '93%',
    },
    ':nth-of-type(4)': {
			fontSize: '102%',
    },
    ':nth-of-type(5)': {
			fontSize: '106%',
			top: '-2px',
			right: '2px',
    },
    ':nth-of-type(6)': {
			fontSize: '103%',
			bottom: '-2px',
    },
    ':nth-of-type(7)': {
			fontSize: '102%',
    },
    ':nth-of-type(8)': {
			fontSize: '98%',
			bottom: '2px',
    },
    ':nth-of-type(9)': {
			fontSize: '101%',
    },
    ':nth-of-type(10)': {
			bottom: '2px',
    },
    ':nth-of-type(11)': {
    },
    ':nth-of-type(12)': {
			fontSize: '105%',
    },
    ':nth-of-type(13)': {
			fontSize: '93%',
    },
    ':nth-of-type(14)': {
			fontSize: '102%',
    },
    ':nth-of-type(15)': {
			top: '-2px',
			right: '2px',
    },
    ':nth-of-type(16)': {
			bottom: '2px',
    },
	},

	MarkdownSourceHeader: {
		position: 'relative',
    ':nth-of-type(2)': {
			fontSize: '105%',
    },
    ':nth-of-type(4)': {
			fontSize: '102%',
    },
    ':nth-of-type(5)': {
			fontSize: '106%',
			right: '2px',
			top: '-2px',
    },
    ':nth-of-type(6)': {
			fontSize: '103%',
			bottom: '1px',
    },
    ':nth-of-type(7)': {
			fontSize: '102%',
    },
    ':nth-of-type(8)': {
			fontSize: '98%',
			bottom: '2px',
    },
    ':nth-of-type(9)': {
			bottom: '2px',
    },
    ':nth-of-type(10)': {
			fontSize: '105%',
			bottom: '2px',
    },
    ':nth-of-type(11)': {
			top: '-1px',
    },
    ':nth-of-type(12)': {
			fontSize: '105%',
    },
    ':nth-of-type(14)': {
			fontSize: '102%',
    },
    ':nth-of-type(15)': {
			top: '-2px',
    },
    ':nth-of-type(16)': {
			fontSize: '103%',
			bottom: '2px',
    },
    ':nth-of-type(17)': {
			fontSize: '102%',
    },
    ':nth-of-type(18)': {
			fontSize: '98%',
			bottom: '2px',
    },
    ':nth-of-type(19)': {
			fontSize: '101%',
    },
    ':nth-of-type(20)': {
			bottom: '2px',
    },
    ':nth-of-type(22)': {
			fontSize: '105%',
    },
    ':nth-of-type(24)': {
			fontSize: '102%',
    },
    ':nth-of-type(25)': {
			top: '-2px',
    },
    ':nth-of-type(26)': {
			fontSize: '103%',
			bottom: '2px',
    },
    ':nth-of-type(27)': {
			fontSize: '102%',
    },
    ':nth-of-type(28)': {
			fontSize: '98%',
			bottom: '1px',
    },
    ':nth-of-type(29)': {
			fontSize: '101%',
    },
    ':nth-of-type(30)': {
			fontSize: '105%',
			bottom: '2px',
    },
    ':nth-of-type(32)': {
			fontSize: '105%',
    },
    ':nth-of-type(34)': {
			fontSize: '102%',
    },
    ':nth-of-type(35)': {
			fontSize: '101%',
			top: '-2px',
    },
    ':nth-of-type(36)': {
			fontSize: '103%',
			bottom: '2px',
    },
    ':nth-of-type(37)': {
			fontSize: '102%',
    },
    ':nth-of-type(38)': {
			fontSize: '98%',
			bottom: '2px',
    },
	},
	ProductShowButton: {
    fontFamily: 'Lobster Hand',
    fontSize: '24px',
    letterSpacing: '1px',
		display: 'inline-block',
    padding: '3px 12px',
    border: 'solid #ff3fb4',
    borderWidth: '3.5px 3px 4px 4px',
    borderTopLeftRadius: '100px 120px',
    borderTopRightRadius: '155px 100px',
    borderBottomRightRadius: '100px 120px',
    borderBottomLeftRadius: '120px 75px',
    transform: 'rotate(.5deg)',
    backgroundColor: 'white',
    color: '#0047ba',
    transition: '0s',
	}


});
