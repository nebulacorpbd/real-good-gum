import React from 'react';
import { StyleSheet, css } from 'aphrodite';
import { ElementPropTypes } from '@volusion/element-proptypes'
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import { block as Block } from '../src/'
import { defaultConfig } from '../src/configs';

import { StyleSheetTestUtils } from 'aphrodite';

describe('The Starter Block', () => {
    StyleSheetTestUtils.suppressStyleInjection();

    it('renders without errors', () => {
      mount(<Block />)
    })

    describe('when there is no custom data', () => {
        it('should render the block with the default content', () => {
        });
    });

    describe('when given custom data', () => {
        it('should render the block using the custom data', () => {
        });
    });
});
