const FontBlock = props => {
  const { utils, data } =  props;
  if(data.isRendering){
    utils.addLink('https://storage.googleapis.com/real-good-gum/css/global.min.css');
  }
  return null;
};
export default FontBlock;
