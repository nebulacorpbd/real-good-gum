import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

class IngredientsBlock extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      sourceList: [
        {
          image: props.sourceOneImage.uriBase + 'c_limit,q_auto,f_auto/' + props.sourceOneImage.imagePath,
          title: props.sourceOneTitle,
          desc: props.sourceOneDesc,
        }, {
          image: props.sourceOneImage.uriBase + 'c_limit,q_auto,f_auto/' + props.sourceTwoImage.imagePath,
          title: props.sourceTwoTitle,
          desc: props.sourceTwoDesc,
        }, {
          image: props.sourceThreeImage.uriBase + 'c_limit,q_auto,f_auto/' + props.sourceThreeImage.imagePath,
          title: props.sourceThreeTitle,
          desc: props.sourceThreeDesc,
        }, {
          image: props.sourceFourImage.uriBase + 'c_limit,q_auto,f_auto/' + props.sourceFourImage.imagePath,
          title: props.sourceFourTitle,
          desc: props.sourceFourDesc,
        }, {
          image: props.sourceFiveImage.uriBase + 'c_limit,q_auto,f_auto/' + props.sourceFiveImage.imagePath,
          title: props.sourceFiveTitle,
          desc: props.sourceFiceDesc,
        }
      ],
      lightbox: false,
      lightboxImage: props.sourceOneImage.uriBase + 'c_limit,q_auto,f_auto/' + props.sourceOneImage.imagePath,
      lightboxTitle: props.sourceOneTitle,
      lightboxDesc: props.sourceOneDesc,
    }
  }
  markdown = (el, style) => {
    return el.split("").map((letter, i) => (
      <span key={i} className={style}>{letter}</span>
    ))
  }

  render() {
    const styles = StyleSheet.create(getStyles({}, this.props));
    return (
      <React.Fragment>
        <div className={css(styles.Wrapper)} id="ingredients">
          <div className={css(styles.Container)}>
            <div className={css(styles.ingredientHeaderFlex)}>
              <h2 className={css(styles.HeaderStyles, styles.ingredientsNonResponsive)}>{this.markdown("   " + this.props.header, css(styles.MarkdownSourceHeader))}</h2>
              <h2 className={css(styles.HeaderStyles, styles.ingredientsResponsive)}>{this.markdown(" NO ASPARTAME. AND", css(styles.MarkdownSourceHeader))}</h2>
              <h2 className={css(styles.HeaderStyles, styles.ingredientsResponsive)}>{this.markdown(" NO PLASTIC EITHER.", css(styles.MarkdownSourceHeader))}</h2>
            </div>
          </div>
          <div className={css(styles.Container)}>
            <p className={css(styles.Paragraph)}>{this.props.desc}</p>
          </div>
          <div className={css(styles.Container)}>
            <ul className={css(styles.SourceMenu)}>
              { this.state.sourceList.map( (item, index) => {
                return (
                  <li key={"_item" + index} className={css(styles.SourceMenuItem)}>
                    <a href="#ingredients"
                        onClick={() => this.setState({lightbox: true, lightboxImage: item.image, lightboxTitle: item.title, lightboxDesc: item.desc})}>
                      <picture className={css(styles.SourceMenuItemImage)}>
                        <source media="(max-width: 480px)" srcSet={item.image} />
                        <source media="(min-width: 481px) and (max-width: 960px)" srcSet={item.image} />
                        <img className={css(styles.SourceMenuItemImage)} src={item.image} />
                      </picture>
                      <h4 className={css(styles.SourceMenuItemTitle)}>
                        <div href="#" className={css(styles.SourceMenuItemTitleLink)}>
                          {this.markdown(item.title, css(styles.Markdown2))}
                        </div>
                      </h4>
                    </a>
                  </li>
                )
              } ) }
            </ul>
          </div>
          <div className={css(styles.Container)}>
            <a className={css(styles.SourceLink)} href="/page/how-we-source">
              {this.markdown("How We Source", css(styles.MarkdownSourceButton))}
            </a>
          </div>
        </div>
        <div className={(this.state.lightbox) ? css(styles.lightboxWrapper) : css(styles.lightboxWrapperOff) }>
          <div className={css(styles.lightboxContainer)}>
            <div className={css(styles.lightboxClose)} onClick={() => this.setState({lightbox: false, lightboxImage: '', lightboxTitle: '', lightboxDesc: ''})}>x</div>
            <picture className={css(styles.lightboxImage)}>
              <source media="(max-width: 480px)" srcSet={this.state.lightboxImage} />
              <source media="(min-width: 481px) and (max-width: 960px)" srcSet={this.state.lightboxImage} />
              <img className={css(styles.lightboxImage)} src={this.state.lightboxImage} />
            </picture>
            <h3 className={css(styles.lightboxTitle)}>{this.markdown(this.state.lightboxTitle, css(styles.MarkdownSourceHeader))}</h3>
            <p className={css(styles.lightboxDesc)}>{this.state.lightboxDesc}</p>
          </div>
        </div>
      </React.Fragment>
    );
  }
}


export default IngredientsBlock;
