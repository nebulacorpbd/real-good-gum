import block from './Block';
import { configSchema, defaultConfig } from './configs';
import { getDataProps } from './getDataProps';

export { block, getDataProps, defaultConfig, configSchema };
