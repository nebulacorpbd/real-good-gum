import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    header: {
        label: 'Header content',
        type: ElementPropTypes.string
    },
    desc: {
        label: 'Paragraph content',
        type: ElementPropTypes.string
    },
    sourceOneImage: {
        label: 'Source 1 Image',
        type: ElementPropTypes.image
    },
    sourceOneTitle: {
        label: 'Source 1 Title',
        type: ElementPropTypes.string
    },
    sourceOneDesc: {
        label: 'Source 1 Description',
        type: ElementPropTypes.string
    },
    sourceTwoImage: {
        label: 'Source 2 Image',
        type: ElementPropTypes.image
    },
    sourceTwoTitle: {
        label: 'Source 2 Title',
        type: ElementPropTypes.string
    },
    sourceTwoDesc: {
        label: 'Source 2 Description',
        type: ElementPropTypes.string
    },
    sourceThreeImage: {
        label: 'Source 3 Image',
        type: ElementPropTypes.image
    },
    sourceThreeTitle: {
        label: 'Source 3 Title',
        type: ElementPropTypes.string
    },
    sourceThreeDesc: {
        label: 'Source 3 Description',
        type: ElementPropTypes.string
    },
    sourceFourImage: {
        label: 'Source 4 Image',
        type: ElementPropTypes.image
    },
    sourceFourTitle: {
        label: 'Source 4 Title',
        type: ElementPropTypes.string
    },
    sourceFourDesc: {
        label: 'Source 4 Description',
        type: ElementPropTypes.string
    },
    sourceFiveImage: {
        label: 'Source 5 Image',
        type: ElementPropTypes.image
    },
    sourceFiveTitle: {
        label: 'Source 5 Title',
        type: ElementPropTypes.string
    },
    sourceFiveDesc: {
        label: 'Source 5 Description',
        type: ElementPropTypes.string
    },
};

export const defaultConfig = {
    header: 'NO ASPARTAME. AND NO PLASTIC EITHER.',
    desc: 'We might be the new gum on the block, but we’re old school when it comes to our ingredients. We make gum the way it used to be made – from a tree. With chicle harvested from rainforest trees along with a delicious blend of organic and natural goodness, our gums are always made from these five simple ingredients:',
    sourceOneImage: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751198/59b6bdbe668bf8001114c08d/rmwop690bbnqn1ic6qxe.png',
    },
    sourceOneTitle: 'Organic Cane Sugar',
    sourceOneDesc: 'Unlike most gums, we never use artificial sweeteners. All our gums get their sweetness from pure, organic cane sugar produced at a family-run plantation in Colombia.',
    sourceTwoImage: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751198/59b6bdbe668bf8001114c08d/nyn34mpbsyjs8hjpl13x.png',
    },
    sourceTwoTitle: 'Wild Harvested Rainforest Chicle',
    sourceTwoDesc: 'Our chicle is harvested by hand from wild sapodilla trees. Over 100 years ago, before modern chemistry, chicle was the original gum base. Our chicle is not certified organic because it comes from the unowned and unmanaged rainforests of Mexico and Central America. Chicle harvesting provides a livelihood for villagers throughout the rustic rainforest regions. Our wild harvested chicle is a sustainable ingredient and also makes Real Good Gum 100% biodegradable.',
    sourceThreeImage: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751195/59b6bdbe668bf8001114c08d/ragiceyxduybyamxivio.png',
    },
    sourceThreeTitle: 'Natural Flavoring',
    sourceThreeDesc: 'All of our flavorings come from the Netherlands and Switzerland – two countries where “natural” actually means something and that don’t mess around when it comes to what’s in their food.',
    sourceFourImage: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751194/59b6bdbe668bf8001114c08d/rrdmp34uzjg4m216j6yv.png',
    },
    sourceFourTitle: 'Organic Glycerin (corn)',
    sourceFourDesc: 'Always made from organic, non-GMO corn from China, glycerin helps to keep our gums moist.',
    sourceFiveImage: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751197/59b6bdbe668bf8001114c08d/di788meydtgz5mzzsvcs.png',
    },
    sourceFiveTitle: 'Organic Sunflower Lecithin',
    sourceFiveDesc: 'Lecithin is an emulsifier, which is a fancy way of saying it keeps all the ingredients of the gum together. Our lecithin comes from Germany and is made from organic non-GMO sunflower seeds.',
};
