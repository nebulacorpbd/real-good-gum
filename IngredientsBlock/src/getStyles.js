export const getStyles = (globalStyles, blockConfig) => ({
	Wrapper: {
		textAlign: 'center',
    padding: '40px 15px 20px',
    maxWidth: '820px',
		margin: 'auto',
	},
	Container: {
		display: 'flex',
    padding: '8px',
	},
	Paragraph: {
    fontSize: '16px',
		fontWeight: 400,
		color: '#0047ba',
    lineHeight: 1.4,
    letterSpacing: '.01em',
    margin: '0 0 12px',
	},
	SourceMenu: {
		display: 'flex',
		margin: 0,
		padding: 0,
		width: '100%',
		listStyle: 'none',
		'@media (max-width: 959px)': {
			display: 'block',
		}
	},
	SourceMenuItem: {
    flex: '1 1 16.6667%',
		maxWidth: '16.6667',
		padding: '14px',
		':hover': {
    	transform: 'rotate(2deg) scale(1.1)',
		}
	},
	SourceMenuItemImage: {
    width: '100px',
    height: 'auto',
	},
	SourceMenuItemTitle: {
		fontFamily: 'Lobster Hand',
    fontWeight: 'normal',
    fontSize: '18px',
    lineHeight: 1,
    letterSpacing: '1px',
    margin: '0 auto',
		':hover': {
			transform: 'rotate(-4deg) scale(1.2)',
		}
	},
	SourceMenuItemTitleLink: {
		color: '#ff3fb4',
	},
	SourceLink: {
    fontSize: '34px',
    margin: '30px auto 10px',
    letterSpacing: '1.5px',
    fontFamily: 'Lobster Hand',
    display: 'inline-block',
    border: 'solid #ff3fb4',
		fontWeight: 700,
    borderWidth: '3.5px 3px 4px 4px',
    padding: '5px 20px 4px 20px',
    borderTopLeftRadius: '100px 120px',
    borderTopRightRadius: '155px 100px',
    borderBottomRightRadius: '100px 120px',
    borderBottomLeftRadius: '120px 75px',
    transform: 'rotate(.5deg)',
    transition: '0s',
    color: '#0047ba',
		':hover': {
    	transform: 'scale(1.1) rotate(-1deg)',
		},
		'@media (min-width: 960px)': {
    	fontSize: '42px',
		}
	},
	ingredientsNonResponsive: {
		'@media only screen and (max-width: 599px)': {
			display: 'none'
		},
	},
	ingredientsResponsive: {
		'@media only screen and (min-width: 600px)': {
			display: 'none'
		},
	},
	HeaderStyles: {
		fontFamily: 'Lobster Hand',
    fontSize: '34px',
    lineHeight: 1.1,
		position: 'relative',
		color: '#FFFFFF',
		backgroundColor: '#0047ba',
    border: 'solid #0047ba',
		border: '3.5px 3px 10px 4px',
    margin: '15px auto 40px',
    textAlign: 'center',
    padding: '4px 20px 2px',
    borderRadius: '50px/60px 40px',
    transform: 'rotate(.5deg)',
    fontStyle: 'normal',
		display: 'inline-block',
		'@media (max-width: 599px)': {
			margin: '-4px 0 0',
			position: 'relative',
		},
		'@media (min-width: 960px)': {
    	fontSize: '42px',
		},
	},
	ingredientHeaderFlex: {
		display: 'flex',
		flexWrap: 'wrap',
		flexDirection: 'column',
		textAlign: 'center',
		padding: '6px 0',
		margin: 'auto',
	},
	lightboxWrapperOff: {
		display: 'none'
	},
	lightboxWrapper: {
		zIndex: 99998,
		backgroundColor: 'rgba(0,0,0,.5)',
		position: 'fixed',
		width: '100%',
		height: '100%',
		top: 0,
		left: 0,
	},
	lightboxContainer: {
		position: 'fixed',
		transform: 'translate(-50%, -50%)',
    backgroundColor: '#0047ba',
		color: '#FFFFFF',
    boxShadow: '0px 2px 6px #000',
    position: 'fixed',
    top: '50%',
    left: '50%',
    maxWidth: '400px',
    padding: '40px',
    width: '90%',
    borderTopLeftRadius: '100px 120px',
    borderTopRightRadius: '155px 100px',
    borderBottomRightRadius: '100px 120px',
    borderBottomLeftRadius: '120px 75px',
		textAlign: 'center',
	},
	lightboxImage: {
		display: 'block',
		filter: 'brightness(0) invert(1)',
		margin: 'auto',
	},
	lightboxName: {
		fontFamily: 'Lobster Hand',
		fontSize: '34px',
		letterSpacing: '1.5px',
		lineHeight: 1.1,
		margin: '24px auto 12px',
	},
	lightboxDesc: {
		fontSize: '16px',
		letterSpacing: '1.5px',
		lineHeight: 1.5,
		margin: '0 0 12px',
	},
	lightboxClose: {
    position: 'absolute',
    right: 0,
    top: 0,
    width: '77px',
    display: 'inline-block',
    padding: '0px 20px 0px 20px',
    borderTopLeftRadius: '100px 120px',
    borderTopRightRadius: '155px 100px',
    borderBottomRightRadius: '100px 120px',
    borderBottomLeftRadius: '120px 75px',
    textAlign: 'center',
    lineHeight: '36px',
    fontSize: '20px',
    color: '#ff3fb4',
    backgroundColor: '#fff',
    transform: 'rotate(1deg)',
		cursor: 'pointer',
	},

	Markdown2: {
		position: 'relative',
    ':nth-of-type(2)': {
			fontSize: '101%',
    },
    ':nth-of-type(3)': {
			fontSize: '93%',
    },
    ':nth-of-type(4)': {
			fontSize: '102%',
    },
    ':nth-of-type(5)': {
			top: '1px',
    },
    ':nth-of-type(6)': {
			bottom: '1px',
    },
    ':nth-of-type(7)': {
			fontSize: '102%',
    },
    ':nth-of-type(8)': {
			fontSize: '98%',
			bottom: '1px',
    },
    ':nth-of-type(9)': {
			fontSize: '101%',
    },
    ':nth-of-type(10)': {
			bottom: '1px',
    },
    ':nth-of-type(11)': {
			top: '-1px',
    },
    ':nth-of-type(12)': {
			fontSize: '105%',
    },
    ':nth-of-type(13)': {
			fontSize: '93%',
    },
    ':nth-of-type(14)': {
			fontSize: '102%',
    },
    ':nth-of-type(15)': {
			top: '-2px',
    },
    ':nth-of-type(16)': {
			bottom: '2px',
    },
    ':nth-of-type(17)': {
			fontSize: '102%',
    },
    ':nth-of-type(18)': {
			fontSize: '98%',
			bottom: '2px',
    },
    ':nth-of-type(19)': {
			fontSize: '101%',
    },
    ':nth-of-type(20)': {
			bottom: '2px',
    },
    ':nth-of-type(23)': {
			fontSize: '93%',
    },
    ':nth-of-type(24)': {
			fontSize: '102%',
    },
    ':nth-of-type(25)': {
			fontSize: '101%',
    },
    ':nth-of-type(26)': {
			fontSize: '103%',
			bottom: '2px',
    },
    ':nth-of-type(27)': {
			fontSize: '102%',
    },
    ':nth-of-type(28)': {
			fontSize: '98%',
			bottom: '1px',
    },
    ':nth-of-type(29)': {
			fontSize: '101%',
    },
    ':nth-of-type(30)': {
			fontSize: '105%',
			bottom: '1px',
    },
	},
	MarkdownSourceHeader: {
		position: 'relative',
    ':nth-of-type(2)': {
			fontSize: '105%',
    },
    ':nth-of-type(4)': {
			fontSize: '102%',
    },
    ':nth-of-type(5)': {
			fontSize: '106%',
			right: '2px',
			top: '-2px',
    },
    ':nth-of-type(6)': {
			fontSize: '103%',
			bottom: '1px',
    },
    ':nth-of-type(7)': {
			fontSize: '102%',
    },
    ':nth-of-type(8)': {
			fontSize: '98%',
			bottom: '2px',
    },
    ':nth-of-type(9)': {
			bottom: '2px',
    },
    ':nth-of-type(10)': {
			fontSize: '105%',
			bottom: '2px',
    },
    ':nth-of-type(11)': {
			top: '-1px',
    },
    ':nth-of-type(12)': {
			fontSize: '105%',
    },
    ':nth-of-type(14)': {
			fontSize: '102%',
    },
    ':nth-of-type(15)': {
			top: '-2px',
    },
    ':nth-of-type(16)': {
			fontSize: '103%',
			bottom: '2px',
    },
    ':nth-of-type(17)': {
			fontSize: '102%',
    },
    ':nth-of-type(18)': {
			fontSize: '98%',
			bottom: '2px',
    },
    ':nth-of-type(19)': {
			fontSize: '101%',
    },
    ':nth-of-type(20)': {
			bottom: '2px',
    },
    ':nth-of-type(22)': {
			fontSize: '105%',
    },
    ':nth-of-type(24)': {
			fontSize: '102%',
    },
    ':nth-of-type(25)': {
			top: '-2px',
    },
    ':nth-of-type(26)': {
			fontSize: '103%',
			bottom: '2px',
    },
    ':nth-of-type(27)': {
			fontSize: '102%',
    },
    ':nth-of-type(28)': {
			fontSize: '98%',
			bottom: '1px',
    },
    ':nth-of-type(29)': {
			fontSize: '101%',
    },
    ':nth-of-type(30)': {
			fontSize: '105%',
			bottom: '2px',
    },
    ':nth-of-type(32)': {
			fontSize: '105%',
    },
    ':nth-of-type(34)': {
			fontSize: '102%',
    },
    ':nth-of-type(35)': {
			fontSize: '101%',
			top: '-2px',
    },
    ':nth-of-type(36)': {
			fontSize: '103%',
			bottom: '2px',
    },
    ':nth-of-type(37)': {
			fontSize: '102%',
    },
    ':nth-of-type(38)': {
			fontSize: '98%',
			bottom: '2px',
    },
	},
	MarkdownSourceButton: {
		position: 'relative',
    ':nth-of-type(2)': {
			fontSize: '105%',
    },
    ':nth-of-type(3)': {
			fontSize: '93%',
    },
    ':nth-of-type(4)': {
			fontSize: '102%',
    },
    ':nth-of-type(5)': {
			top: '-2px',
    },
    ':nth-of-type(6)': {
			fontSize: '103%',
			bottom: '2px',
    },
    ':nth-of-type(7)': {
			fontSize: '102%',
    },
    ':nth-of-type(8)': {
			fontSize: '98%',
			bottom: '2px',
    },
    ':nth-of-type(9)': {
			fontSize: '101%',
    },
    ':nth-of-type(10)': {
			bottom: '2px',
    },
    ':nth-of-type(11)': {
			top: '-4px',
    },
    ':nth-of-type(12)': {
			fontSize: '105%',
    },
    ':nth-of-type(13)': {
			fontSize: '93%',
    },
	},
})
