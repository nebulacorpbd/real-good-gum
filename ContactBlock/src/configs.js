import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    background: {
        label: 'Section Background',
        type: ElementPropTypes.image
    },
    facebookLink: {
        label: 'Facebook Link',
        type: ElementPropTypes.string
    },
    facebookIcon: {
        label: 'Facebook Icon',
        type: ElementPropTypes.image
    },
    instagramLink: {
        label: 'Instagram Link',
        type: ElementPropTypes.string
    },
    instagramIcon: {
        label: 'Instagram Icon',
        type: ElementPropTypes.image
    },
    divider: {
        label: 'Divider image',
        type: ElementPropTypes.image
    },
};

export const defaultConfig = {
    background: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/',
      imagePath: 'image/upload/v1507751199/59b6bdbe668bf8001114c08d/dfkcwfwlr5g7xbabajab.png',
      fullSizeSrc: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/v1507751199/59b6bdbe668bf8001114c08d/dfkcwfwlr5g7xbabajab.png',
    },
    facebookLink: 'https://www.facebook.com/RealGoodGum/',
    facebookIcon: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/',
      imagePath: 'image/upload/v1507751199/59b6bdbe668bf8001114c08d/izdiowk5epyvelgiwjrs.png',
    },
    instagramLink: 'https://www.instagram.com/realgoodgum/',
    instagramIcon: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/',
      imagePath: 'image/upload/v1507751198/59b6bdbe668bf8001114c08d/osj5hbdk88bg4axizkli.png',
    },
    divider: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/',
      imagePath: 'image/upload/v1505160348/59b6bdbe668bf8001114c08d/v6y2weppelbirlqwca1b.png',
    },
};
