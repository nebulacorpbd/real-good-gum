import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

function ContactBlock(props) {
  const styles = StyleSheet.create(getStyles({}, props));
  return (
    <React.Fragment>
      <div className={css(styles.Wrapper)} id="contact">
        <picture className={css(styles.ObjectFit)}>
            <source media="(max-width: 480px)" srcset={props.background.uriBase + 'w_480,h_605,c_fill,q_auto:good,f_auto/' + props.background.imagePath} />
            <source media="(min-width: 481px) and (max-width: 960px)" srcset={props.background.uriBase + 'w_960,h_605,c_fill,q_auto:good,f_auto/' + props.background.imagePath} />
            <img class="_1gbjpc9" src={props.background.uriBase + 'c_limit,q_auto:good,f_auto/' + props.background.imagePath} />
        </picture>
        <div className={css(styles.Container)}>
          <div className={css(styles.Column4)}>
            <h3 className={css(styles.Header)}>CHEW</h3>
            <h4 className={css(styles.Header, styles.HeaderSmall)}>OUR EAR OFF.</h4>
          </div>
          <div className={css(styles.Column8)}>
            <p className={css(styles.Paragraph)}>We always love to talk about gum. For questions, comments, press and retailer inquiries, email us at <a href="mailto:Info@RealGoodGum.com" className={css(styles.Link)}>Info@RealGoodGum.com</a></p>
            <div className={css(styles.PaddingRes)}>
              <h5 className={css(styles.Header5)}>Interested in our wholesale program?</h5>
              <p className={css(styles.Paragraph)}><a href="/page/wholesale-registration" className={css(styles.Link)}>Register here</a> for more information.</p>
            </div>
          </div>
        </div>
        <div className={css(styles.Container)}>
          <div className={css(styles.Column12)}>
            <p className={css(styles.Paragraph)}><img src={props.divider.uriBase + 'q_auto:eco,f_auto/' + props.divider.imagePath} /></p>
          </div>
        </div>
        <div className={css(styles.Container)}>
          <div className={css(styles.Column12)}>
            <h4 className={css(styles.Header4)}>FOLLOW THE GOOD:</h4>
            <ul className={css(styles.ContactSocial)}>
              <li className={css(styles.ContactSocialItem)}>
                <a href={props.facebookLink} className={css(styles.ContactSocialItemLink)} target={props.facebookLink}>
                  <picture>
                    <source media="(max-width: 480px)" srcset={props.facebookIcon.uriBase + 'w_50,c_limit,q_auto,f_auto/' + props.facebookIcon.imagePath} />
                    <source media="(min-width: 481px) and (max-width: 960px)" srcset={props.facebookIcon.uriBase + 'w_50,c_limit,q_auto,f_auto/' + props.facebookIcon.imagePath} />
                    <img src={props.facebookIcon.uriBase + 'w_50,c_limit,q_auto,f_auto/' + props.facebookIcon.imagePath} alt="Like us on Facebook" />
                  </picture>
                </a>
              </li>
              <li className={css(styles.ContactSocialItem)}>
                <a href={props.instagramLink} className={css(styles.ContactSocialItemLink)} target={props.instagramLink}>
                  <picture>
                    <source media="(max-width: 480px)" srcset={props.instagramIcon.uriBase + 'w_50,c_limit,q_auto,f_auto/' + props.instagramIcon.imagePath} />
                    <source media="(min-width: 481px) and (max-width: 960px)" srcset={props.instagramIcon.uriBase + 'w_50,c_limit,q_auto,f_auto/' + props.instagramIcon.imagePath} />
                    <img src={props.instagramIcon.uriBase + 'w_50,c_limit,q_auto,f_auto/' + props.instagramIcon.imagePath} alt="Follow us on Instagram" />
                  </picture>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

ContactBlock.defaultProps = defaultConfig;

export default ContactBlock;
