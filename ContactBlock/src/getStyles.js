export const getStyles = (globalStyles, blockConfig) => ({
	Wrapper: {
		position: 'relative',
    padding: '65px 0 65px',
    backgroundColor: "#ffe900",
		zIndex: 1,
	},
	ObjectFit: {
		objectFit: 'cover',
    objectPosition: 'center',
    height: '100%',
    width: '100%',
    maxWidth: 'unset',
    position: 'absolute',
    top: 0,
    left: 0,
		zIndex: -1,
	},
	Container: {
		display: 'flex',
    padding: '8px',
    maxWidth: '820px',
		margin: 'auto',
		'@media (max-width: 768px)': {
      display: 'block',
			textAlign: 'center',
		},
	},
  Column4: {
    flex: '1 1 33.333333%',
    width: '33.333333%',
		textAlign: 'center',
		'@media (max-width: 768px)': {
	    flex: '1 1 100%',
	    width: '100%',
		},
  },
  Column8: {
    flex: '1 1 66.666667%',
    width: '66.666667%',
		textAlign: 'center',
		'@media (max-width: 768px)': {
	    flex: '1 1 100%',
	    width: '100%',
		},
  },
  Column12: {
    flex: '1 1 100%',
    width: '100%',
		textAlign: 'center'
  },
	Header: {
		fontFamily: 'Lobster Hand',
    fontSize: '70px',
    lineHeight: '.8em',
		color: '#0047ba',
    fontWeight: 700,
    margin: '0px auto',
	},
	HeaderSmall: {
    fontSize: '48px',
    lineHeight: '1.1em',
    margin: '0px auto 12px',
	},
	Header4: {
		fontFamily: 'Lobster Hand',
		color: '#0047ba',
		backgroundColor: '#FFFFFF',
		display: 'inline-block',
    padding: '0px 20px 0px 20px',
    border: 'solid #fff',
    borderWidth: '4.5px 3px 3px 4px',
    borderTopLeftRadius: '100px 120px',
    borderTopRightRadius: '155px 100px',
    borderBottomRightRadius: '100px 120px',
    borderBottomLeftRadius: '120px 75px',
    transform: 'rotate(1deg)',
		fontSize: '28px',
		fontWeight: 400,
    lineHeight: '1.1em',
    margin: '0px auto 20px',
		'@media (min-width: 600px)': {
			fontSize: '48px',
		}
	},
	PaddingRes: {
		padding: '0 25px',
	},
	Header5: {
		fontFamily: 'Lobster Hand',
		color: '#0047ba',
    fontWeight: 400,
    fontSize: '23px',
    lineHeight: '1.1em',
		letterSpacing: '1.5px',
    margin: '0px auto 12px',
		'@media (maxWidth: 599px)': {
    	padding: '0 30px',
		}
	},
	Paragraph: {
		fontFamily: 'Nunito',
		fontSize: '16px',
		lineHeight: '1.4',
		fontWeight: '400',
		letterSpacing: '.01em',
    padding: '0 25px',
    margin: '0 auto 20px',
		color: '#0047ba',
	},
	Link: {
		color: '#3cd52e',
		':hover': {
			textDecoration: 'underline',
		},
	},
	ContactSocial: {
		margin: 0,
		padding: 0,
		listStyle: 'none',
	},
	ContactSocialItem: {
		display: 'inline-block',
	},
	ContactSocialItemLink: {
		display: 'inline-block',
		width: '50px',
		padding: '2px',
		':hover': {
			transform: 'scale(1.5) rotate(-1.5deg)',

		}
	}
});
