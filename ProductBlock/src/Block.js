import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

class ProductBlock extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: props.data,
      productId: null,
      quantity: 1,
      variantId: null,
      itemPrice: 0.00,
      productImage: '',
      relatedProducts: [],
    }
  }
  handleQuantity = (e) => {
    const { value } = e.target;
    this.setState({ quantity: parseInt(value) });
  };

  componentDidMount() {
    this.props.utils.addScript('https://cdn.jsdelivr.net/npm/easyzoom@2.5.2/dist/easyzoom.js');
    this.props.utils.addScript('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js');
    const {data} = this.state;
    if ( data.images != undefined ) this.setState({productImage: data.images[0].uriBase + 'c_limit,q_auto,f_auto/' + data.images[0].imagePath});
    if ( data.id != null && data.productVariants[0].id != null && data.productVariants[0].price != null ) {
      this.setState({
        productId: data.id,
        variantId: data.productVariants[0].id,
        itemPrice: data.productVariants[0].price
      });
      setTimeout(() => {
        this.loadImages();
      }, 500);
    }
    this.props.utils.client.products.getRelatedById(data.id)
    .then(data => {
      this.setState({relatedProducts: data});
    });

    const style = document.createElement("style");
    const css = ".slick-list{overflow: auto;height: auto !important;}.slick-track {display: -webkit-flex;display: flex;-webkit-flex-direction: row;flex-direction: row;-webkit-justify-content: center;justify-content: center;-webkit-align-content: center;align-content: center;-webkit-align-items: center;align-items: center;} .slick-dots li button {width: 14px;height: 14px;background: #0047ba;border-radius: 100%;opacity: .5;} .slick-active button { opacity: 1 !important; } .easyzoom {height: 500px;display: flex;align-items: center;} .easyzoom-flyout img{max-width: unset !important;}";
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
    document.body.appendChild(style);
  }

  loadImages = () => {
    const {utils} = this.props;
    if (typeof jQuery !== 'undefined') {
      if (typeof EasyZoom !== 'undefined') {
        if (window.innerWidth >= 600) {
          jQuery('.easyzoom').easyZoom();
        } else {
          jQuery('.gallery-items').slick({
            adaptiveHeight: true,
            arrows: false,
            dots: true,
          });
        }
      } else {
        setTimeout(() => {
          this.loadImages();
        }, 1000);
      }
    } else {
      setTimeout(() => {
        this.loadImages();
      }, 1000);
    }
  }
  addToCart = (productId, quantity, variantId, itemPrice) => {
    this.props.utils.pubSub.publish(this.props.utils.events.cart.addToCart, {
      productId, // type: string, from product data
      quantity, // type: int, whatever quantity you want to add to the cart, ex: 1
      variantId, // type: string, from product data
      itemPrice, // type: float, from product data, ex: 4.99
    })
  }

  handleChange = (d) => {
    this.setState({productImage: d});
    jQuery('.easyzoom').easyZoom().data('easyZoom').swap(d, d);
  }

  render() {
    const styles = StyleSheet.create(getStyles({}, this.props));
    let { data, relatedProducts, productId, quantity, variantId, itemPrice } = this.state;
    if ( Object.keys(data).length > 0 ) {
      return (
          <React.Fragment>
            <div className={css(styles.Container)}>
              <div className={css(styles.ContentWrapper)}>
                <div className={css(styles.GalleryWrapper)}>
                  <div className={css(styles.SelectedImage)}>
                    <div className="easyzoom easyzoom--overlay">
                      <a href={this.state.productImage}>
                        <img src={this.state.productImage} data-src={this.state.productImage} className={css(styles.SelectedImageItem)} />
                      </a>
                    </div>
                  </div>
                  <ul className={"thumbnails " + css(styles.GalleryImages)}>
                    {
                      data.images.map( (d, index) => {
                        return (
                          <li className={css(styles.GalleryImageSingle)} key={index}>
                            <a
                              data-standard={d.uriBase + 'w_160,c_limit,q_auto,f_auto/' + d.imagePath}
                              onClick={() => this.handleChange(d.uriBase + 'q_auto,f_auto/' + d.imagePath)}><img src={d.uriBase + 'w_160,c_limit,q_auto,f_auto/' + d.imagePath} className={css(styles.SelectedImageItem)} /></a>
                          </li>
                        )
                      })
                    }
                  </ul>
                </div>
                <div className={css(styles.DescWrapper)}>
                  <h1 className={css(styles.ProductTitle)}>{data.name}</h1>
                  <div className={css(styles.ProductPrice)}>${data.price}</div>
                  <div className={css(styles.ResponsiveGallery)}>
                    <div className='gallery-items'>
                      {
                        data.images.map( (d, index) => {
                          return (
                            <div key={index}>
                              <img src={d.uriBase + 'w_400,c_limit,q_auto:eco,f_auto/' + d.imagePath} />
                            </div>
                          )
                        })
                      }
                    </div>
                  </div>
                  <div className={css(styles.ProductDesc)} dangerouslySetInnerHTML={{ __html: ( data.description != undefined ) ?  data.description.replace('<h1>', '<h1 class='+css(styles.ProductTitle)+">").replace('<h3>', '<h3 class='+css(styles.h3)+">") : '' }}></div>
                  <div className={css(styles.ProductVariations)}>
                    {
                      (data.productVariants != undefined) ?
                      data.productVariants.map( d => {
                        return (
                          <fieldset key={d.id} className={css(styles.inputGroup)}>
                            <input
                              className={css(styles.inputRadio)}
                              type="radio" value={d.price}
                              id="SelectRadio" name={"variation" + d.id}
                              checked={this.state.variantId === d.id}
                              onChange={() => this.setState({variantId: d.id, itemPrice: d.price})} />
                            <label className={css(styles.inputLabel)} htmlFor={d.id}
                              onClick={() => this.setState({variantId: d.id, itemPrice: d.price})}>
                              {d.variants[0] + " $" + d.price}
                            </label>
                          </fieldset>
                        )
                      }) : ''
                    }
                  </div>
                  <div className={css(styles.ProductQuantity)}>
                    <label className={css(styles.inputLabelQuanity)} htmlFor=''>Quantity</label>
                    <input className={css(styles.inputQuantity)} type="number" name="quantity" id="quantity"  min="1" value={this.state.quantity} onChange={this.handleQuantity} />
                  </div>
                  <input
                    type="submit"
                    className={css(styles.PorductSubmit)}
                    value="ADD TO CART"
                    onClick={() => this.addToCart(this.state.productId, this.state.quantity, this.state.variantId, this.state.itemPrice ) }
                    // onClick={() => addToCart(data.id) }
                    />
                </div>
              </div>
              <div className={css(styles.SidebarWrapper)}>
                <h2 className={css(styles.SidebarTitle)}>
                  YOU MIGHT ALSO LIKE
                </h2>
                <ul className={css(styles.productsWrapper)}>
                  {relatedProducts.map( (item, index) => {
                    return <li key={index} className={css(styles.ProductCardItem)}>
                      <a href={"/p/" + item.seo_friendlyName} className={css(styles.ProductCardLink)}>
                        <span className={css(styles.ProductCardLinkText)}>DETAILS</span>
                      </a>
                      <div className={css(styles.ProductCardWrapper)}>
                        <div>
                          <img src={(item.images[0].uriBase != null ) ? item.images[0].uriBase + 'w_450,c_limit,q_auto,f_auto/' + item.images[0].imagePath : ""} className={css(styles.ProductCardImage)} />
                        </div>
                        <div className={css(styles.ProductCardDescription)}>
                          <div className={css(styles.ProductCardTitle)}>{item.name}</div>
                          <div className={css(styles.ProductCardPrice)}>${item.price}</div>
                          <div className={css(styles.ProductCardOptions)}>{(item.variantOptions.length > 0)? "More options available":""}</div>
                        </div>
                      </div>
                    </li>
                  })}
                </ul>
              </div>
            </div>
          </React.Fragment>
      );
    } else {
      return (
          <React.Fragment>
            <div className={css(styles.Container, styles.center)}>
              <h1 className={css(styles.ErrorTitle)}>Product Not Found</h1>
            </div>
          </React.Fragment>
        )
    }
  }
}

export default ProductBlock;
