export const getDataProps = ( utils, { productSlug } ) => {
	return utils.client.products
	.getBySlug(productSlug)
	.then(product=>{
    utils.addScript('https://code.jquery.com/jquery-2.2.4.min.js');
    utils.addLink('https://cdn.jsdelivr.net/npm/easyzoom@2.5.2/css/easyzoom.css');
    utils.addLink('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
    utils.addLink('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css');

    return product;
  })
	.catch(console.error);
};
