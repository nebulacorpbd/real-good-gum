export const getStyles = (globalStyles, blockConfig) => ({
	Container: {
		display: 'flex',
    padding: '8px',
    maxWidth: '100vw',
		margin: 'auto',
		'@media (max-width: 1279px)': {
      display: 'block',
		},
		'@media (min-width: 960px) and (max-width: 1279px)': {
      maxWidth: '960px',
		},
	},
  ContentWrapper: {
    flex: '1 1 70%',
    maxWidth: '70%',
		display: 'flex',
    padding: '8px',
		margin: 'auto',
    '@media (max-width: 1279px)': {
      flex: '1 1 100%',
      maxWidth: '100%',
    },
    '@media (max-width: 959px)': {
      display: 'block',
    }
  },
  SidebarWrapper: {
    flex: '1 1 25%',
    maxWidth: '25%',
    padding: '20px 40px 0',
    textAlign: 'center',
    '@media (min-width: 1280px)': {
      marginLeft: '5%',
      borderLeft: '1px solid #0047ba',
    },
    '@media (max-width: 1279px)': {
      borderTop: '1px solid #0047ba',
      flex: '1 1 100%',
      maxWidth: '100%',
    }
  },
  GalleryWrapper: {
    flex: '1 1 50%',
    maxWidth: '50%',
		padding: '8px',
    '@media (max-width: 960px)': {
      flex: '1 1 100%',
      width: '100%',
      maxWidth: '100%',
    },
    '@media (max-width: 599px)': {
			display: 'none',
    }
  },
	GalleryImages: {
		listStyle: 'none',
		padding: 0,
		margin: 0,
		textAlign: 'center',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    alignContent: 'center',
	},
	GalleryImageSingle: {
		display: 'inline-block',
    maxWidth: '25%',
	},
	ResponsiveGallery: {
		'@media (min-width: 600px)': {
			display: 'none',
		}
	},
	SelectedImage: {
		height: '500px',
		overflow: 'hidden',
		display: 'flex',
		alignItems: 'center',
	},
	SelectedImageItem: {
		maxWidth: '100%',
	},
  DescWrapper: {
    flex: '1 1 50%',
    maxWidth: '50%',
    color: '#0047ba',
    margin: 0,
		padding: '8px',
    '@media (max-width: 960px)': {
      flex: '1 1 100%',
      width: '100%',
      maxWidth: '100%',
    }
  },
  ProductTitle: {
    fontFamily: '"Lobster Hand", sans-serif',
    fontSize: '42px',
    lineHeight: '1.1',
    letterSpacing: '1.5px',
    margin: '24px 0 12px',
  },
  ErrorTitle: {
    fontFamily: '"Lobster Hand", sans-serif',
    fontSize: '42px',
    lineHeight: '1.1',
    color: '#0047ba',
    letterSpacing: '1.5px',
		textAlign: 'center',
    margin: '12px 0',
  },
  ProductPrice: {
    fontFamily: 'Nunito',
    fontSize: '34px',
    margin: '0 0 12px',
  },
  ProductDesc: {
    fontFamily: 'Nunito',
    fontSize: '16px',
    lineHeight: '1.4',
    padding: '15px 0',
    margin: '0 0 12px',
  },
  ProductVariations: {
    padding: '25px 0',
  },
  inputGroup: {
    display: 'block',
    position: 'relative',
    border: '0',
    padding: '0',
    marginBottom: '5px',
  },
  inputLabel: {
    fontFamily: '"Nunito",sans-serif,"google"',
    color: '#ff3fb4',
    fontWeight: 700,
    display: 'inline-block',
    lineHeight: '18px',
    fontSize: '18px',
    padding: '0 0 0 10px',
    lineHeight: 1.4,
    letterSpacing: '.01em'
  },
  inputLabelQuanity: {
    fontFamily: '"Nunito",sans-serif,"google"',
    fontWeight: 900,
    display: 'block',
    fontWeight: 'bold',
    letterSpacing: '.095em',
    lineHeight: '1.4',
    textTransform: 'uppercase',
    fontSize: '19px',
    color: '#0047ba',
    maxWidth: '100%',
    marginBottom: '5px',
    fontWeight: 'bold',
  },
  ProductQuantity: {
    padding: '15px 0',
  },
  inputQuantity: {
    width: '100px',
    display: 'block',
    width: 'auto',
    height: '38px',
    padding: '0 10px',
    fontSize: '16px',
    lineHeight: '1.5',
    color: '#555',
    backgroundColor: '#fff',
    backgroundImage: 'none',
    border: '1px solid #ccc',
    borderRadius: 0,
  },
  PorductSubmit: {
    fontFamily: 'Lobster Hand',
    fontSize: '24px',
    letterSpacing: '1px',
    padding: '15px 25px',
    lineHeight: '1.333333',
    margin: '15px 0',
    backgroundColor: '#FFFFFF',
    color: '#0047ba',
    transition: '0s',
    borderColor: '#ff3fb4',
    cursor: 'pointer',
    display: 'inline-block',
    border: 'solid #ff3fb4',
    borderWidth: '3.5px 3px 4px 4px',
    borderTopLeftRadius: '100px 120px',
    borderTopRightRadius: '155px 100px',
    borderBottomRightRadius: '100px 120px',
    borderBottomLeftRadius: '120px 75px',
    transform: 'rotate(.5deg)',
		outline: 'none',
    ':hover': {
      transform: 'scale(1.1) rotate(-1deg)',
    }
  },
  SidebarTitle: {
    fontFamily: 'Lobster Hand',
    color: '#0047ba',
    fontSize: '34px',
    lineHeight: '1.5',
    margin: '24px auto 12px',
    padding: '0',
  },
  center: {
    textAlign: 'center'
  },
  h3: {
    fontFamily: '"Lobster Hand", sans-serif',
    fontSize: '28px',
    letterSpacing: '1.5px',
    margin: '24px 0 12px',
  },

  productsWrapper: {
    padding: '8px',
    display: 'flex',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    flexDirection: 'row',
    textAlign: 'center',
    '@media (max-width: 599px)': {
      display: 'block',
    }
  },
  ProductCardItem: {
    display: 'block',
    textAlign: 'center',
    marginBottom: '30px',
    padding: '8px',
    position: 'relative',
    '@media (max-width: 1279px) and (min-width: 960px)': {
      flex: '1 1 25%',
      maxWidth: '25%',
    },
    '@media (max-width: 959px) and (min-width: 600px)': {
			flex: '1 1 50%',
			maxWidth: '50%',
    },
  },
  ProductCardImage: {
    marginBottom: '15px',
    maxHeight: '300px',
    maxWidth: '100%',
    border: 0,
    margin: 'auto',
    transition: 'opacity .2s ease',
  },
  ProductCardTitle: {
    fontSize: '24px',
    fontFamily: 'Lobster Hand',
    letterSpacing: '1.5px',
    fontWeight: 700,
    lineHeight: 1.5,
    color: '#0047ba',
  },
  ProductCardPrice: {
    fontSize: '19px',
    color: '#0047ba',
    lineHeight: 1.4,
  },
  ProductCardOptions: {
    fontSize: '87%',
    color: '#0047ba',
  },
  ProductCardLink: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    color: '#ff3fb4',
    width: '100%',
    height: '100%',
    margin: 'auto',
    transition: 'background .2s ease',
    opacity: 0,
    ':hover': {
      background: 'rgba(255,255,255,.82)',
      opacity: 1,
    }
  },
  ProductCardLinkText: {
    fontFamily: 'Lobster Hand',
    letterSpacing: '2px',
    fontSize: '30px',
    position: 'absolute',
    top: '40%',
    right: 0,
    left: 0,
    display: 'block',
    transform: 'translate3d(0, -50%, 0)',
    opacity: 1,
  },
})
