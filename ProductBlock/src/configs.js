import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
  productSlug: {
    label: 'Product Slug URI Identifier',
    type: ElementPropTypes.readOnly,
    isPrivate: true,
  },
};

export const defaultConfig = {
  productSlug: 'pageVar:pageUrlText'
};
