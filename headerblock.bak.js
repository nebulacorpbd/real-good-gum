import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

function StarterBlock(props) {
    const data = [];
    if ( props.items != null ) {
      const data = props.items[0].items;
    }
    const logoUri = props.logo;
    const styles = StyleSheet.create(getStyles({}, props));

    return (
        <React.Fragment>
          <div className={css(styles.headerWrapper)}>
              <a href="/" className={css(styles.logoWrapper)}>
                <img
                  className={css(styles.logo)}
                  src="https://res.cloudinary.com/dyx4yhvoq/image/upload/f_auto,q_auto/v1554404386/59b6bdbe668bf8001114c08d/rxjgcqe8gac0udhs1jrh.png" />
                <img
                  className={css(styles.logoAlt)}
                  src="https://res.cloudinary.com/dyx4yhvoq/image/upload/v1505837509/59b6bdbe668bf8001114c08d/hwjamrqezc5p1hbbqopo.png" />
              </a>
              <ul className={css(styles.menuWrapper)}>
                {data.map( (item, index) => {
                  return <li key={index} className={index !== 2 ? css(styles.menuItem) : css(styles.menuItemRight)}>
                    <a href={`$(item.id)`} className={css(styles.menuLink)}>
                      {item.name}
                    </a>
                  </li>;
                })}
                <li className={css(styles.menuItem, styles.menuCart)}>
                  <a href="#" className={css(styles.menuLink)} onClick={() => this.addItem({productId: "123", quantity: 1, variantId: "123", itemPrice: 100.10})}>
                    <span className="fa fa-shopping-basket"></span> {this.state.itemAddedCount}
                  </a>
                </li>
              </ul>
            </div>
        </React.Fragment>
    );
}
StarterBlock.defaultProps = defaultConfig;

export default StarterBlock;
