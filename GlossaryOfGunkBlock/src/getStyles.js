export const getStyles = (globalStyles, blockConfig) => ({
	Wrapper: {
    padding: '0 0 30px',
	},
	Container: {
		display: 'flex',
    padding: '8px',
    maxWidth: '820px',
		margin: 'auto',
		'@media (max-width: 960px)': {
      display: 'block',
		},
	},
  Column12: {
    flex: '1 1 100%%',
    width: '100%',
		textAlign: 'center'
  },
	PageHeader: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#0047ba',
		fontWeight: 400,
    fontSize: '42px',
		paddingBottom: '10px',
    lineHeight: '1.1em',
    fontStyle: 'normal',
		textAlign: 'center',
    margin: '24px auto 12px',
    boxSizing: 'border-box',
	},
	SubHeaderLeft: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#ff3fb4',
    fontSize: '36px',
    lineHeight: '1.1em',
    fontWeight: 700,
    textAlign: 'left',
    fontStyle: 'normal',
    margin: '24px auto 12px',
    boxSizing: 'border-box',
	},
	ReferenceLinkWrapper: {
    textAlign: 'left',
    padding: '0',
    margin: 0,
    listStyle: 'none',
  },
	ReferenceLinkItem: {
    marginBottom: '20px',
    marginLeft: '15px',
    textIndent: '-14px',
    fontSize: '14px',
  },
	ReferenceLink: {
    color: '#0047ba',
    fontWeight: 700,
    margin: '24px 0 12px',
		fontSize: '16px',
    lineHeight: '1.4',
    letterSpacing: '.16px',
    boxSizing: 'border-box',
    fontSize: '14px',
    textDecoration: 'underline',
    ':hover': {
      color: '#ff3fb4',
    },
		'@media (max-width: 768px)': {
    	margin: '12px auto',
    	lineHeight: '160%',
		}
	},
  ReferenceNo: {
    color: '#0047ba',
    fontWeight: 700,
    marginRight: '5px',
    fontSize: '65%',
    top: '-.5em',
  }
});
