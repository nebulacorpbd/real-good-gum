import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    image: {
        label: 'Image',
        type: ElementPropTypes.image
    }
};

export const defaultConfig = {
    image: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1506631442/59b6bdbe668bf8001114c08d/eclbbilttjgeqpcs8vk9.jpg',
    },
};
