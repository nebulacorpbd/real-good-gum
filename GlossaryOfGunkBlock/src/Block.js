import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

function Block(props) {
  const styles = StyleSheet.create(getStyles({}, props));

  return (
    <React.Fragment>
      <div className={css(styles.Wrapper)}>
        <div className={css(styles.Container)}>
          <div className={css(styles.Column12)}>
            <h1 className={css(styles.PageHeader)}>Glossary Of Gunk</h1>
            <picture className={css(styles.ProductShowThumbnail)}>
              <source media="(max-width: 480px)" srcSet={props.image.uriBase + 'q_auto,f_auto/' + props.image.imagePath } />
              <source media="(min-width: 481px) and (max-width: 960px)" srcSet={props.image.uriBase + 'q_auto,f_auto/' + props.image.imagePath } />
              <img src={props.image.uriBase + 'q_auto,f_auto/' + props.image.imagePath } />
            </picture>
            <h2 className={css(styles.SubHeaderLeft)}>
              References
            </h2>
            <ol className={css(styles.ReferenceLinkWrapper)}>
              <li className={css(styles.ReferenceLinkItem)}>
                <sup className={css(styles.ReferenceNo)}>1</sup>
                <a  className={css(styles.ReferenceLink)}
                    href="http://www.nyc.gov/html/doh/downloads/pdf/eode/turf_report_05-08.pdf">
                    http://www.nyc.gov/html/doh/downloads/pdf/eode/turf_report_05-08.pdf
                </a>
              </li>
              <li className={css(styles.ReferenceLinkItem)}>
                <sup className={css(styles.ReferenceNo)}>2</sup>
                <a  className={css(styles.ReferenceLink)}
                    href="http://healthwyze.org/index.php/component/content/article/383-why-a-stick-of-gum-is-more-harmful-to-your-healththan-anything-that-you-eat.html">
                     http://healthwyze.org/index.php/component/content/article/383-why-a-stick-of-gum-is-more-harmful-to-your-healththan-anything-that-you-eat.html
                </a>
              </li>
              <li className={css(styles.ReferenceLinkItem)}>
                <sup className={css(styles.ReferenceNo)}>3</sup>
                <a  className={css(styles.ReferenceLink)}
                    href="http://www.canada.com/montrealgazette/news/books/story.html?id=c44590c2-3ab9-470e-92ec-81f6c1d2151a">
                    http://www.canada.com/montrealgazette/news/books/story.html?id=c44590c2-3ab9-470e-92ec-81f6c1d2151a
                </a>
              </li>
              <li className={css(styles.ReferenceLinkItem)}>
                <sup className={css(styles.ReferenceNo)}>4</sup>
                <a  className={css(styles.ReferenceLink)}
                    href="http://cspinet.org/new/pdf/food-dyes-rainbow-of-risks.pdf">
                    http://cspinet.org/new/pdf/food-dyes-rainbow-of-risks.pdf
                </a>
              </li>
              <li className={css(styles.ReferenceLinkItem)}>
                <sup className={css(styles.ReferenceNo)}>5</sup>
                <a  className={css(styles.ReferenceLink)}
                    href="http://www.dailymail.co.uk/news/article-2290544/Aspartame-Cancer-premature-birth-fears-linked-zzy-drinksweetener.html">
                    http://www.dailymail.co.uk/news/article-2290544/Aspartame-Cancer-premature-birth-fears-linked-zzy-drinksweetener.html
                </a>
              </li>
              <li className={css(styles.ReferenceLinkItem)}>
                <sup className={css(styles.ReferenceNo)}>6</sup>
                <a  className={css(styles.ReferenceLink)}
                    href="http://healthwyze.org/index.php/component/content/article/383-why-a-stick-of-gum-is-more-harmful-to-your-healththan-anything-that-you-eat.html">
                    http://healthwyze.org/index.php/component/content/article/383-why-a-stick-of-gum-is-more-harmful-to-your-healththan-anything-that-you-eat.html
                </a>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

Block.defaultProps = defaultConfig;

export default Block;
