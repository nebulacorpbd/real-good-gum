import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    headerPartOne: {
        label: 'Header Part 1',
        type: ElementPropTypes.string
    },
    headerPartTwo: {
        label: 'Header Part 1',
        type: ElementPropTypes.string
    },
    text: {
        label: 'Text content',
        type: ElementPropTypes.string
    },
    background: {
        label: 'Background',
        type: ElementPropTypes.image
    }
};

export const defaultConfig = {
    headerPartOne: 'GUM FOR THE',
    headerPartTwo: 'WHOLE FAMILY.',
    text: 'We were founded by a single dad of three great kids. One of his children had a health issue that required a medication. The side effects to the medication were so severe that this child lost the ability to talk. The doctors suggested having her chew gum. But conventional gum caused her to have seizures and other side effects. So this dad looked for another option in chewing gum but couldn’t find one. So he made one that was fun enough for his child and everyone else. The child is now recovered and doing great and now we make Real Good Gum so everyone can have a better choice!',
    background: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1507751194/59b6bdbe668bf8001114c08d/pebzymhjdjlrtpnm11oy.png'
    },
};
