export const getStyles = (globalStyles, blockConfig) => ({
	Wrapper: {
    padding: '65px 0',
    backgroundColor: "#3cd52e	",
		position: 'relative',
		zIndex: 1,
	},
	ObjectFit: {
		objectFit: 'cover',
    objectPosition: 'center',
    height: '100%',
    width: '100%',
    maxWidth: 'unset',
    position: 'absolute',
    top: 0,
    left: 0,
		zIndex: -1,
	},
	Container: {
		display: 'flex',
    padding: '8px',
    maxWidth: '820px',
		margin: 'auto',
		'@media (max-width: 768px)': {
      display: 'block',
		},
	},
  Column12: {
    flex: '1 1 100%%',
    width: '100%',
		textAlign: 'center'
  },
	Header: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#0047ba',
    fontSize: '42px',
    lineHeight: '1.1em',
    fontWeight: 700,
    fontStyle: 'normal',
		textAlign: 'center',
    marginTop: '10px',
    padding: '0px 20px 0px 20px',
    border: 'solid #fff',
    borderWidth: '4.5px 3px 3px 4px',
    borderTopLeftRadius: '100px 120px',
    borderTopRightRadius: '155px 100px',
    borderBottomRightRadius: '100px 120px',
    borderBottomLeftRadius: '120px 75px',
    transform: 'rotate(1deg)',
    backgroundColor: '#fff',
		display: 'none',
	},
	NonResponsive: {
		'@media (min-width: 769px)': {
			display: 'inline-block',
	    marginBottom: '50px',
		}
	},
	Responsive: {
		'@media (max-width: 768px)': {
			display: 'inline-block',
	    margin: '10px auto',
		}
	},
	Paragraph: {
		color: '#FFF',
    marginBottom: '17px',
    paddingLeft: '10px',
    paddingRight: '10px',
    width: '75%',
    margin: '0 auto',
		fontFamily: 'Nunito',
		fontSize: '16px',
    lineHeight: '1.4',
		fontWeight: '700',
    letterSpacing: '.01em',
		'@media (max-width: 768px)': {
    	margin: '12px auto',
			width: '90%',
    	lineHeight: '160%',
		}
	},
	Link: {
		color: '#ffe900 !important',
		':hover': {
			textDecoration: 'underline',
		},
		'@media (max-width: 768px)': {
    	marginLeft: 'auto',
			width: '90%',
		}
	},
	SectionButton: {
    fontFamily: 'Lobster Hand',
    letterSpacing: '1px',
    fontSize: '24px',
		display: 'inline-block',
    paddingLeft: '10px',
    paddingRight: '10px',
    marginTop: '12px',
    marginBottom: '12px',
    lineHeight: 1.1,
    color: '#ffe900',
    width: '80%',
		':hover': {
    	textDecoration: 'underline',
    	transform: 'scale(1.1) rotate(-1deg)',
		}
	}
});
