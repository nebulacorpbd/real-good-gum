import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

function StoryBlock(props) {
  const styles = StyleSheet.create(getStyles({}, props));

  return (
    <React.Fragment>
      <div className={css(styles.Wrapper)} id="story">
        <picture className={css(styles.ObjectFit)}>
          <source media="(max-width: 480px)" srcset={props.background.uriBase + 'w_480,h_531,c_fill,q_auto:good,f_auto/' + props.background.imagePath} />
          <source media="(min-width: 481px) and (max-width: 960px)" srcset={props.background.uriBase + 'w_960,h_531,c_fill,q_auto:good,f_auto/' + props.background.imagePath} />
          <img className={css(styles.ObjectFit)} src={props.background.uriBase + 'c_limit,q_auto:good,f_auto/' + props.background.imagePath} />
        </picture>
        <div className={css(styles.Container)}>
          <div className={css(styles.Column12)}>
            <h3 className={css(styles.Header, styles.NonResponsive)}>{props.headerPartOne} {props.headerPartTwo}</h3>
            <h3 className={css(styles.Header, styles.Responsive)}>{props.headerPartOne}</h3>
            <h3 className={css(styles.Header, styles.Responsive)}>{props.headerPartTwo}</h3>
            <p className={css(styles.Paragraph)}>
              {props.text}
            </p>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

StoryBlock.defaultProps = defaultConfig;

export default StoryBlock;
