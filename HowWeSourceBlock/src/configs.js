import { ElementPropTypes } from '@volusion/element-proptypes';

export const configSchema = {
    divider: {
        label: 'Divider',
        type: ElementPropTypes.image
    }
};

export const defaultConfig = {
    divider: {
      uriBase: 'https://res.cloudinary.com/dyx4yhvoq/image/upload/',
      imagePath: 'v1505160348/59b6bdbe668bf8001114c08d/v6y2weppelbirlqwca1b.png',
    }
};
