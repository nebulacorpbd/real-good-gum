export const getStyles = (globalStyles, blockConfig) => ({
	Wrapper: {
    padding: '30px 0',
	},
	Container: {
		display: 'flex',
    padding: '8px',
    maxWidth: '820px',
		margin: 'auto',
		'@media (max-width: 960px)': {
      display: 'block',
		},
	},
  Column12: {
    flex: '1 1 100%%',
    width: '100%',
		textAlign: 'center'
  },
	PageHeader: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#0047ba',
    fontSize: '42px',
    lineHeight: '1.1em',
    fontWeight: 700,
    fontStyle: 'normal',
		textAlign: 'center',
    margin: '24px auto 12px',
    boxSizing: 'border-box',
	},
	SubHeader: {
		fontFamily: 'Lobster Hand',
    lineHeight: '1.1em',
		color: '#ff3fb4',
    fontSize: '36px',
    lineHeight: '1.1em',
    fontWeight: 700,
    fontStyle: 'normal',
		textAlign: 'center',
    margin: '24px auto 40px',
    boxSizing: 'border-box',
	},
	Paragraph: {
    margin: '12px 0 40px',
		fontSize: '16px',
    lineHeight: '1.4',
    letterSpacing: '.16px',
    boxSizing: 'border-box',
		color: '#0047ba',
		'@media (max-width: 768px)': {
    	margin: '12px auto',
    	lineHeight: '160%',
		}
	},
	ParagraphBold: {
		color: '#ff3fb4',
    margin: '24px 0 12px',
		fontSize: '16px',
    lineHeight: '1.4',
		fontWeight: '600',
    letterSpacing: '1.6px',
    boxSizing: 'border-box',
		'@media (max-width: 768px)': {
    	margin: '12px auto',
    	lineHeight: '160%',
		}
	},
});
