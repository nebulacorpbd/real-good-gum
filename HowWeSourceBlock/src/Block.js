import React from 'react';
import { StyleSheet, css } from 'aphrodite';

import { defaultConfig } from './configs';
import { getStyles } from './getStyles';

function Block(props) {
  const styles = StyleSheet.create(getStyles({}, props));

  return (
    <React.Fragment>
      <div className={css(styles.Wrapper)}>
        <div className={css(styles.Container)}>
          <div className={css(styles.Column12)}>
            <h1 className={css(styles.PageHeader)}>How We Source</h1>
            <p className={css(styles.Paragraph)}>
              We use a combination of organic, wild harvested and natural ingredients. These three different ingredient classifications are based on how and where we source them. To find them, we literally searched all over the world.
            </p>
            <h2 className={css(styles.SubHeader)}>THREE WAYS TO SAY GOOD.</h2>
            <p className={css(styles.Paragraph)}>
              <img src={props.divider.uriBase + 'q_auto,f_auto/' + props.divider.imagePath} />
            </p>
            <p className={css(styles.ParagraphBold)}>
              CERTIFIED ORGANIC (Sugar, Glycerin & Lecithin)
            </p>
            <p className={css(styles.Paragraph)}>
              We use a combination of organic, wild harvested and natural ingredients. These three different ingredient classifications are based on how and where we source them. To find them, we literally searched all over the world.
            </p>
            <p className={css(styles.ParagraphBold)}>
              WILD HARVESTED (Chicle)
            </p>
            <p className={css(styles.Paragraph)}>
              A “wild” ingredient is as natural as natural gets. Wild simply means anything that is grown as nature intended – in a mixed species, unpolluted habitat and harvested sustainably. If collected from approved organically managed land, wild ingredients can also be certified organic. Our wild chicle is harvested by hand from sapodilla trees located deep inside the Mexican rainforest. The only reason it’s not also certified organic is because the rainforest isn’t owned or officially managed and therefore can’t go through the certification process.
            </p>
            <p className={css(styles.ParagraphBold)}>
              NATURAL (Flavoring)
            </p>
            <p className={css(styles.Paragraph)}>
              The term “natural” in the US can mean very little since many products can claim they’re natural even if they’re not. However, our natural flavorings come from Switzerland and the Netherlands – both are countries that comply with the EU’s strict regulatory guidelines for flavorings. In the EU, an ingredient can only be labeled “natural” if it truly is natural. We also chose natural flavoring certified by the EU over organic flavoring to limit the amount of unwanted plant byproducts that occur naturally in the production of organic flavorings. So while our flavors aren’t organic, we’re confident they do meet the highest standards in the world for flavoring.
            </p>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

Block.defaultProps = defaultConfig;

export default Block;
